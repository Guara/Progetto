package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.cards.EscapeHatchCards;

import org.junit.Test;

public class EscapeHatchCardTest {


    /**
     * Test for methods isGreen and isRed
     */
	@org.junit.Test
	public void testIsGreenIsRed() {
		EscapeHatchCards scialuppa = new EscapeHatchCards("green");
		assertEquals(true, scialuppa.isGreen());
		assertEquals(false, scialuppa.isRed());
	}
}
