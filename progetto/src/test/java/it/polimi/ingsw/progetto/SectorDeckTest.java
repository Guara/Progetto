package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;

import it.polimi.ingsw.progetto.model.cards.SectorCards;
import it.polimi.ingsw.progetto.model.decks.SectorDeck;
import org.junit.Test;

public class SectorDeckTest {

    /**
     * Test for method setSectorDeck
     */
    @org.junit.Test
    public void testSetSectorDeck() {
        SectorDeck mazzo = new SectorDeck();
        mazzo.setSectorDeck();
        SectorCards carta = new SectorCards("fakenoise");
        int finemazzo = 25;
        int contatore = 0;
        for (int i = 0; i < finemazzo; i++) {
            carta = mazzo.getCard();
            if ((carta.getType() == 1 || carta.getType() == 2) && carta.getObjectIcon()) {
                contatore++;
            }
        }
        assertEquals(8, contatore);
    }
}
