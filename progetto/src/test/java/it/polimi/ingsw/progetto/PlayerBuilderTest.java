package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.helpers.PlayerBuilder;
import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;
import it.polimi.ingsw.progetto.model.players.AlienPlayer;
import it.polimi.ingsw.progetto.model.players.Players;

import org.junit.Test;

public class PlayerBuilderTest {

    /**
     * Test for build: it verifies the correct creation of new players
     */
	@org.junit.Test
	public void testBuild() {
		PlayerTypeCards tipo = new PlayerTypeCards();
		PlayerBuilder builder = new PlayerBuilder();
		builder.setCard(tipo);
		builder.setName("alieno");
		Players alieno = builder.build();
		assertEquals(true, alieno instanceof AlienPlayer);
	}

}
