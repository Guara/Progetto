package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.StateGame;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.cards.SectorCards;
import it.polimi.ingsw.progetto.model.decks.ObjectDeck;
import it.polimi.ingsw.progetto.model.decks.SectorDeck;
import it.polimi.ingsw.progetto.model.map.Board;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.players.AlienPlayer;
import it.polimi.ingsw.progetto.model.players.HumanPlayer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class StateGameTest {

    /**
     * Test for method getPlayerByID
     */
	@org.junit.Test
	public void testGetPlayerByID() {
        StateGame model= new StateGame();
		AlienPlayer alieno = new AlienPlayer("alieno");
        alieno.setId(1);
        model.setPlayers(alieno);
		int id = alieno.getID();
		assertEquals(alieno, model.getPlayerById(id));
	}

    /**
     * Test for method setObjectDiscarded
     */
	@org.junit.Test
	public void testSetObjectDiscarded() {
		ObjectCards attacco = new ObjectCards("attack");
		ObjectDeck mazzo = new ObjectDeck();
		StateGame model = new StateGame();
		model.setObjectDiscarded(attacco.getType());
		mazzo = model.getObjectDiscarded();
		assertEquals(0, mazzo.getCard().getType());
	}

    /**
     * Test for drawSectorCard
     */
	@org.junit.Test
	public void testDrawSectorCard() {
		SectorDeck mazzo = new SectorDeck();
		SectorCards carta = new SectorCards("truenoise");
		mazzo.addSectorCard(carta);
		SectorCards seconda = new SectorCards("silence");
		mazzo.addSectorCard(seconda);
		StateGame model = new StateGame();
		model.setSectorDeck(mazzo);
		assertEquals(carta, model.drawSectorCard());
	}

    /**
     * Test for method getPlayerOnSector: it creates a temporary map, it adds two players on the map and creates an arbitrary list, then it verifies if board.getPlayerOnSector returns the right list
     */
	@org.junit.Test
	public void testGetPlayerOnSector() {
		StateGame model = new StateGame();
		Board mappa = model.getMap();
		AlienPlayer alieno = new AlienPlayer("alieno");
		HumanPlayer umano = new HumanPlayer("umano");
		List<Integer> listamappa = new ArrayList<Integer>();
		listamappa.add(alieno.getID());
		listamappa.add(umano.getID());
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(alieno.getID());
		Position corrente = new Position('l', 12);
		mappa.put(corrente, listamappa);
		alieno.setPosition(corrente);
		Position nuova = new Position('l', 13);
		mappa.setPosition(alieno, nuova);
		assertEquals(lista, model.getPlayerOnSector(nuova));
	}

    /**
     * Test for method getName
     */
	@org.junit.Test
	public void testGetName() {
		StateGame model = new StateGame();
		AlienPlayer alieno = new AlienPlayer("alieno");
		HumanPlayer umano = new HumanPlayer("umano");
		model.setPlayers(alieno);
		model.setPlayers(umano);
		List<String> lista = new ArrayList<String>();
		lista.add("alieno");
		lista.add("umano");
		assertEquals(lista, model.getName());
	}

    /**
     * Test for method limitObjectsOwned
     */
	@org.junit.Test
	public void testLimitObjectsOwned() {
		StateGame model = new StateGame();
		HumanPlayer umano = new HumanPlayer("umano");
		model.setPlayers(umano);
		ObjectDeck mazzo = new ObjectDeck();
		mazzo.createObjectDeck();
		model.setObjectDeck(mazzo);
		for(int i = 0; i<4; i++) {
		model.getPlayer().setObjectsOwned(model.drawObjectCard());
		}
		assertEquals(true, model.limitObjectsOwned(umano));
	}

    /**
     * Test for method alienAttack: it creates a situation where an alien (added to the map) attack an human (it could be an alien even) and verifies that the human is dead after board.alienAttack
     */
    @org.junit.Test
    public void testAlienAttack() {
        StateGame model = new StateGame();
        HumanPlayer umano = new HumanPlayer("umano");
        umano.setId(1);
        AlienPlayer alieno = new AlienPlayer("alieno");
        alieno.setId(2);
        Position corrente = new Position('l', 10);
        Position nuova = new Position('l', 11);
        List<Integer> lista = new ArrayList<Integer>();
        model.setPlayers(umano);
        model.setPlayers(alieno);
        model.setCurrentPlayer(alieno.getID());
        umano.setPosition(corrente);
        lista.add(umano.getID());
        alieno.setPosition(corrente);
        lista.add(alieno.getID());
        model.getMap().put(corrente, lista);
        model.setPosition(umano, nuova);
        model.setPosition(alieno, nuova);
        model.alienAttack();
        assertEquals(true, umano.getPlayersDeath());
    }

}
