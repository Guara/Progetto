package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.map.Position;

import it.polimi.ingsw.progetto.model.map.SectorsType;
import org.junit.Test;

public class PositionTest {

    /**
     * Test for method setPosition
     */
	@org.junit.Test
	public void testSetPosition() {
		Position corrente = new Position('l', 10);
		Position verifica = corrente;
		verifica.setColumn('m');
		verifica.setLine(13);
		corrente.setPosition(109, 13);
		assertEquals(verifica, corrente);
	}

    /**
     * Test for methods getLine and setLine
     */
    @org.junit.Test
    public void testGetSetLine() {
        Position corrente = new Position('l', 11);
        corrente.setLine(12);
        assertEquals(12, corrente.getLine());
    }

    /**
     * Test for methods getColumn and setColumn
     */
    @org.junit.Test
    public void testGetSetColumn() {
        Position corrente = new Position('f', 4);
        corrente.setColumn('g');
        assertEquals('g', corrente.getColumn());
    }

    /**
     * Test for methods getSectorType and setSectorType
     */
    @org.junit.Test
    public void testGetSetSectorType() {
        Position corrente = new Position('g', 5);
        corrente.setSectorType(SectorsType.DANGEROUS);
        assertEquals(SectorsType.DANGEROUS, corrente.getSectorType());
    }

    /**
     * Test for methods getEscapeHatchBroken and setEscapeHatchBroken
     */
    @org.junit.Test
    public void testGetSetEscapeHatchBroken() {
        Position corrente = new Position('v', 2);
        corrente.setEscapeHatchBroken(true);
        assertEquals(true, corrente.getEscapeHatchBroken());
    }

}
