package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.players.AlienPlayer;
import it.polimi.ingsw.progetto.model.players.HumanPlayer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class PlayerTest {

	@org.junit.Test
	public void testAlienName() {
		AlienPlayer alieno = new AlienPlayer("alieno");
		assertEquals("alieno", alieno.getName());
	}
   
	@org.junit.Test
	public void testHumanName() {
		HumanPlayer umano = new HumanPlayer("umano");
		assertEquals("umano", umano.getName());
	}

    /**
     * Test for method getPosition
     */
	@org.junit.Test
	public void testGetPosition() {
		AlienPlayer alieno = new AlienPlayer("alieno");
		Position posizione = new Position('f', 9);
		alieno.setPosition(posizione);
		assertEquals(posizione, alieno.getPosition());
	}

    /**
     * Test for method getHIstoryPosition
     */
	@org.junit.Test
	public void testGetHistoryPosition() {
		AlienPlayer alieno = new AlienPlayer("alieno");
		List<Position> lista = new ArrayList<Position>();
		Position prima = new Position('f', 9);
		lista.add(prima);
		alieno.setHistoryPosition(prima);
		Position seconda = new Position('f', 11);
		lista.add(seconda);
		alieno.setHistoryPosition(seconda);
		Position terza = new Position('g', 13);
		lista.add(terza);
		alieno.setHistoryPosition(terza);
		assertEquals(lista, alieno.getHistoryPosition());
	}

    /**
     * Test for method useObjectCard: it verifies that an objectcard is removed after that it's used
     */
	@org.junit.Test
	public void testUseObjectCard() {
		HumanPlayer umano = new HumanPlayer("umano");
		ObjectCards attacco = new ObjectCards("attack");
		List<ObjectCards> possedute = new ArrayList<ObjectCards>();
		umano.setObjectsOwned(attacco);
		ObjectCards difesa = new ObjectCards("defense");
		umano.setObjectsOwned(difesa);
		ObjectCards adrenalina = new ObjectCards("adrenaline");
		umano.setObjectsOwned(adrenalina);
		possedute = umano.getObjectsOwned();
		ObjectCards altroattacco = new ObjectCards("attacco");
		umano.setObjectsOwned(altroattacco);
		umano.useObjectCard(altroattacco.getType());
		assertEquals(possedute, umano.getObjectsOwned());
	}


    /**
     * Test for method getLastPosition
     */
	@org.junit.Test
	public void testGetLastPosition() {
		HumanPlayer umano = new HumanPlayer("umano");
		Position posizione = new Position('e', 4);
		umano.setHistoryPosition(posizione);
		posizione = new Position('d', 3);
		umano.setHistoryPosition(posizione);
		posizione = new Position('c', 3);
		umano.setHistoryPosition(posizione);
		assertEquals(posizione, umano.getLastPosition());
	}

    /**
     * Test for method getDefense
     */
	@org.junit.Test
	public void testGetDefense() {
		HumanPlayer umano = new HumanPlayer("umano");
		umano.setDefense(true);
		Boolean difesa = umano.getDefense();
		assertEquals(true, difesa);
	}

    /**
     * Test for method getAdrenaline
     */
	@org.junit.Test
	public void testGetAdrenaline() {
		HumanPlayer umano = new HumanPlayer("umano");
		umano.setAdrenaline(true);
		assertEquals(true, umano.getAdrenaline());
	}

    /**
     * Test for method getObjectsOwned
     */
	@org.junit.Test
	public void testGetObjectsOwned() {
		HumanPlayer umano = new HumanPlayer("umano");
		List<ObjectCards> lista = new ArrayList<ObjectCards>();
		ObjectCards oggetto = new ObjectCards("attack");
		lista.add(oggetto);
		umano.setObjectsOwned(oggetto);
		oggetto = new ObjectCards("defense");
		lista.add(oggetto);
		umano.setObjectsOwned(oggetto);
		oggetto = new ObjectCards("adrenaline");
		lista.add(oggetto);
		umano.setObjectsOwned(oggetto);
		oggetto = new ObjectCards("attack");
		lista.add(oggetto);
		umano.setObjectsOwned(oggetto);
		assertEquals(lista, umano.getObjectsOwned());
	}

    /**
     * Test for objectCardNumber: it verifies the correct return of the number of objects owned by a player creating an arbitrary list of objects owned
     */
	@org.junit.Test
	public void testObjectCardNumber() {
		HumanPlayer umano = new HumanPlayer("umano");
		ObjectCards oggetto = new ObjectCards("attack");
		umano.setObjectsOwned(oggetto);
		oggetto = new ObjectCards("defense");
		umano.setObjectsOwned(oggetto);
		oggetto = new ObjectCards("adrenaline");;
		umano.setObjectsOwned(oggetto);
		oggetto = new ObjectCards("attack");
		umano.setObjectsOwned(oggetto);
		assertEquals(4, umano.objectCardNumber());
	}

}
