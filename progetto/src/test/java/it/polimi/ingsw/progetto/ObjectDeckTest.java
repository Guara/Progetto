package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.decks.ObjectDeck;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ObjectDeckTest {

    /**
     * Test for method getCard
     */
	@org.junit.Test
	public void testGetCard() {
		ObjectDeck mazzo = new ObjectDeck();
		mazzo.createObjectDeck();
		mazzo.getCard();
		assertEquals(11, mazzo.getArray().size());
	}

    /**
     * Test for method add
     */
	@org.junit.Test
	public void testAdd() {
		ObjectDeck mazzo = new ObjectDeck();
		List<ObjectCards> lista = new ArrayList<ObjectCards>();
		ObjectCards carta = new ObjectCards("attack");
		lista.add(carta);
		mazzo.add(carta);
		assertEquals(lista, mazzo.getArray());
	}

}
