package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;
import it.polimi.ingsw.progetto.model.decks.PlayerTypeDeck;

import org.junit.Test;

public class PlayerTypeDeckTest {

    /**
     * Test for method setPlayerTypeDeck
     */
	@org.junit.Test
	public void testSetPlayerTypeDeck() {
		int playersnumber = 4;
		PlayerTypeDeck mazzo = new PlayerTypeDeck(playersnumber);
		assertEquals(4, mazzo.getArray().size());
	}

    /**
     * Test for method drawPlayerTypeCard
     */
	@org.junit.Test
	public void testDrawPlayerTypeCard() {
		PlayerTypeDeck mazzo = new PlayerTypeDeck(4);
		PlayerTypeCards carta = new PlayerTypeCards();
		carta = mazzo.drawPlayerTypeCard(mazzo.getArray());
		assertEquals(3, mazzo.getArray().size());
		assertNotEquals(carta, mazzo.drawPlayerTypeCard(mazzo.getArray()));
	}
}
