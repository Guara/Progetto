package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.cards.SectorCards;

import org.junit.Test;

public class SectorCardTest {

    /**
     * Test for method getTypeString
     */
	@org.junit.Test
	public void testGetTypeString() {
		SectorCards carta = new SectorCards("truenoise");
		assertEquals("Rumore nel tuo settore", carta.getTypeString());
		carta = new SectorCards("nothing");
		assertEquals("", carta.getTypeString());
	}

    /**
     * Test for method setObjectIcon
     */
	@org.junit.Test
	public void testSetObjectIcon() {
		SectorCards carta = new SectorCards("truenoise");
		carta.setObjectIcon(true);
		assertEquals(true, carta.getObjectIcon());
	}
}
