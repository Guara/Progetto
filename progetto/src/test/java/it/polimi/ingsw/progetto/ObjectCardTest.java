package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;

import org.junit.Test;

public class ObjectCardTest {

    /**
     * Test for method getType
     */
	@org.junit.Test
	public void testGetType() {
		ObjectCards oggetto = new ObjectCards("sedative");
		assertEquals(2, oggetto.getType());
		oggetto = new ObjectCards("teleport");
		assertEquals(1, oggetto.getType());
		oggetto = new ObjectCards("defense");
		assertEquals(4, oggetto.getType());
		oggetto = new ObjectCards("attack");
		assertEquals(0, oggetto.getType());
		oggetto = new ObjectCards("adrenaline");
		assertEquals(5, oggetto.getType());
	}
}
