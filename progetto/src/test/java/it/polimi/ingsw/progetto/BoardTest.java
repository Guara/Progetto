package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.map.Board;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.map.SectorsType;
import it.polimi.ingsw.progetto.model.players.AlienPlayer;
import it.polimi.ingsw.progetto.model.players.HumanPlayer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class BoardTest {

    /**
     * Test for the method setSector: it takes some positions and calling board.setSector it verifies if the SectorType is correctly setted
     */
	@org.junit.Test
	public void testSetSector() {
		Board mappa = new Board();
		Position posizione = new Position('i', 4);
		mappa.setSector(posizione);
		assertEquals(SectorsType.DANGEROUS, posizione.getSectorType());
		posizione = new Position('o', 9);
		mappa.setSector(posizione);
		assertEquals(SectorsType.SECURE, posizione.getSectorType());
		posizione = new Position('v', 2);
		mappa.setSector(posizione);
		assertEquals(SectorsType.ESCAPEHATCH, posizione.getSectorType());
	}

    /**
     * Test for method checkLimits
     */
	@org.junit.Test
	public void testCheckLimits() {
		Board mappa = new Board();
		Boolean prova = mappa.checkLimits(100, 6);
		assertEquals(false, prova);
		prova = mappa.checkLimits(100, 5);
		assertEquals(true, prova);
	}

    /**
     * Test for method setPosition: it sets two players (one human and one alien) and tries board.setPosition with some positions to coverage most of the possible movements
     */

	@org.junit.Test
	public void testSetPosition() {
		Board mappaP = new Board();
		HumanPlayer umanoP = new HumanPlayer("um");
        AlienPlayer alienoP = new AlienPlayer("al");
		Position correnteP = new Position('l', 8);
		Position nuovaP = new Position('k', 9);
		List<Integer> lista = new ArrayList<Integer>();
		lista.add(umanoP.getID());
		umanoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(umanoP, nuovaP);
		assertEquals(nuovaP, umanoP.getPosition());
        correnteP = new Position('v', 4);
		nuovaP = new Position('w', 5);
		umanoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(umanoP, nuovaP);
		assertEquals(nuovaP, umanoP.getPosition());
        correnteP = new Position('q', 6);
		nuovaP = new Position('q', 7);
		umanoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(umanoP, nuovaP);
		assertEquals(nuovaP, umanoP.getPosition());
		correnteP = new Position('a', 10);
		nuovaP = new Position('a', 9);
		umanoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(umanoP, nuovaP);
		assertEquals(nuovaP, umanoP.getPosition());
		correnteP = new Position('f', 4);
		nuovaP = new Position('e', 4);
		umanoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(umanoP, nuovaP);
		assertEquals(nuovaP, umanoP.getPosition());
		correnteP = new Position('b', 3);
		nuovaP = new Position('c', 3);
		umanoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(umanoP, nuovaP);
		assertEquals(nuovaP, umanoP.getPosition());
		lista = new ArrayList<Integer>();
		lista.add(alienoP.getID());
		correnteP = new Position('j', 8);
		nuovaP = new Position('k', 10);
		alienoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('i', 10);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		correnteP = nuovaP;
		nuovaP = new Position('i', 8);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		correnteP = new Position('f', 6);
		nuovaP = new Position('e', 5);
		alienoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('g', 5);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		correnteP = new Position('o', 9);
		nuovaP = new Position('q', 9);
		alienoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('m', 8);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		alienoP.setAlienFeeding(true);
		correnteP = new Position('g', 7);
		nuovaP = new Position('d', 8);
		alienoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('g', 10);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('d', 5);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('j', 6);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('f', 9);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		nuovaP = new Position('h', 9);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		correnteP = new Position('p', 6);
		nuovaP = new Position('s', 6);
		alienoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		correnteP = new Position('t', 5);
		nuovaP = new Position('u', 3);
		alienoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
		correnteP = new Position('s', 2);
		nuovaP = new Position('s', 4);
		alienoP.setPosition(correnteP);
		mappaP.put(correnteP, lista);
		mappaP.setPosition(alienoP, nuovaP);
		assertEquals(nuovaP, alienoP.getPosition());
		mappaP.setPosition(alienoP, correnteP);
		assertEquals(correnteP, alienoP.getPosition());
        correnteP = new Position('d', 10);
        nuovaP = new Position('b', 8);
        alienoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
        assertEquals(nuovaP, alienoP.getPosition());
        mappaP.setPosition(alienoP, correnteP);
        assertEquals(correnteP, alienoP.getPosition());
        correnteP = new Position('n', 10);
        nuovaP = new Position('k', 11);
        alienoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
        assertEquals(nuovaP, alienoP.getPosition());
        mappaP.setPosition(alienoP, correnteP);
        assertEquals(correnteP, alienoP.getPosition());
        correnteP = new Position('j', 5);
        nuovaP = new Position('i', 7);
        alienoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
        assertEquals(nuovaP, alienoP.getPosition());
        mappaP.setPosition(alienoP,correnteP);
        assertEquals(correnteP, alienoP.getPosition());
        correnteP = new Position('m', 6);
        nuovaP = new Position('o', 7);
        alienoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
        assertEquals(nuovaP, alienoP.getPosition());
        mappaP.setPosition(alienoP, correnteP);
        assertEquals(correnteP, alienoP.getPosition());
        nuovaP = new Position('o', 8);
        mappaP.setPosition(alienoP, nuovaP);
        correnteP = new Position('v', 3);
        nuovaP = new Position('w', 3);
        umanoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(umanoP, nuovaP);
        assertEquals(nuovaP, umanoP.getPosition());
        correnteP = new Position('w', 13);
        nuovaP = new Position('v', 10);
        alienoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
        assertEquals(nuovaP, alienoP.getPosition());
        mappaP.setPosition(alienoP, correnteP);
        assertEquals(correnteP, alienoP.getPosition());
        correnteP = new Position('u', 12);
        nuovaP = new Position('w', 11);
        alienoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
        assertEquals(nuovaP, alienoP.getPosition());
        mappaP.setPosition(alienoP, correnteP);
        assertEquals(correnteP, alienoP.getPosition());
        correnteP = new Position('n', 5);
        nuovaP = new Position('p', 4);
        alienoP.setPosition(correnteP);
        mappaP.put(correnteP, lista);
        mappaP.setPosition(alienoP, nuovaP);
        assertEquals(nuovaP, alienoP.getPosition());
        mappaP.setPosition(alienoP, correnteP);
        assertEquals(correnteP, alienoP.getPosition());
	}

    /**
     * Test for method removeIDFromThisPosition: it creates an arbitrary list of id and creates a situation where it's necessary to use board.removeIDFromThisPosition; then it compares the result with the arbitrary list
     */
	@org.junit.Test
	public void testRemoveIDFromThisPosition() {
		Board mappa = new Board();
		HumanPlayer umano = new HumanPlayer("umano");
		List<Integer> lista = new ArrayList<Integer>();
		Integer id = umano.getID();
		lista.add(id);
		HumanPlayer human = new HumanPlayer("human");
		Integer idi = human.getID();
		lista.add(idi);
		Position posizione = new Position('k', 8);
		mappa.put(posizione, lista);
		mappa.removeIDFromThisPosition(posizione, idi);
		lista.remove(idi);
		assertEquals(lista, mappa.getPlayersInThisPosition(posizione));
	}

    /**
     * Test for method checkCrossingFor3Steps: it creates some situations where it's necessary to use board.checkCrossingFor3Steps and it verifies the correct return
     */
	@org.junit.Test
	public void testCheckCrossingFor3Steps() {
		Board mappa = new Board();
		Position corrente = new Position('j', 11);
		Position nuova = new Position('j', 8);
		assertEquals(true, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(true, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('j', 10);
		nuova = new Position('g', 12);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('i', 5);
		nuova = new Position('f', 6);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('j', 6);
		nuova = new Position('g', 5);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('f', 6);
		nuova = new Position('c', 7);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('q', 4);
		nuova = new Position('n', 3);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('t', 11);
		nuova = new Position('q', 11);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('h', 6);
		nuova = new Position('k', 8);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('g', 6);
		nuova = new Position('j', 5);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('r', 9);
		nuova = new Position('u', 10);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('q', 11);
		nuova = new Position('t', 11);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		corrente = new Position('m', 6);
		nuova = new Position('n', 8);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('s', 5);
		nuova = new Position('t', 2);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('r', 9);
		nuova = new Position('s', 12);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('h', 6);
		nuova = new Position('i', 4);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('n', 4);
		nuova = new Position('p', 2);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
		corrente = new Position('s', 9);
		nuova = new Position('u', 11);
		assertEquals(false, mappa.checkCrossingFor3Steps(corrente, nuova));
		assertEquals(false, mappa.checkCrossingFor3Steps(nuova, corrente));
	}

    /**
     * Test for method checkCrossingToGetAroundObstacles: it creates some situations where it's necessary to use board.checkCrossingToGetAroundObstacles and it verifies the correct return
     */
	@org.junit.Test
	public void testCheckCrossingToGetAroundObstacles() {
		Board mappa = new Board();
		Position corrente = new Position('m', 8);
		Position nuova = new Position('m', 6);
		Boolean verifica = mappa.checkCrossingToGetAroundObstacles(corrente, nuova);
		assertEquals(false, verifica);
		corrente = new Position('s', 2);
		nuova = new Position('s', 4);
		verifica = mappa.checkCrossingToGetAroundObstacles(corrente, nuova);
		assertEquals(true, verifica);
	}


    /**
     * Test for method checkExistingPosition: it checks the current return of board.checkExistingPosition creating an example
     */
    @org.junit.Test
    public void testCheckExistingPosition() {
        Board mappa = new Board();
        AlienPlayer alieno = new AlienPlayer("alieno");
        HumanPlayer umano = new HumanPlayer("umano");
        List<Integer> lista = new ArrayList<Integer>();
        lista.add(alieno.getID());
        lista.add(umano.getID());
        Position corrente = new Position('j', 9);
        Position nuova = new Position('j', 10);
        alieno.setPosition(corrente);
        mappa.put(corrente, lista);
        mappa.setPosition(alieno, nuova);
        umano.setPosition(corrente);
        assertEquals(true, mappa.checkExistingPosition(umano, nuova));
    }

}
