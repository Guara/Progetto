package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;

import it.polimi.ingsw.progetto.controller.Controller;
import it.polimi.ingsw.progetto.model.StateGame;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.players.HumanPlayer;
import it.polimi.ingsw.progetto.view.View;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ControllerTest {

    /**
     * Test for method onAttackCard: it verifies that the objectcard  attack is correctly used
     */
    @org.junit.Test
    public void testOnAttackCard() {
        StateGame model = new StateGame();
        View view = new View();
        Controller controllo = new Controller(view, model);
        HumanPlayer umanoA = new HumanPlayer("a");
        HumanPlayer umanoB = new HumanPlayer("b");
        Position corrente = new Position('q', 8);
        ObjectCards carta = new ObjectCards("attack");
        List<Integer> lista = new ArrayList<Integer>();
        umanoA.setId(1);
        umanoB.setId(2);
        model.setPlayers(umanoA);
        model.setPlayers(umanoB);
        umanoA.setPosition(corrente);
        umanoB.setPosition(corrente);
        lista.add(umanoA.getID());
        lista.add(umanoB.getID());
        model.getMap().put(corrente, lista);
        model.setCurrentPlayer(umanoA.getID());
        umanoA.setObjectsOwned(carta);
        controllo.onAttackCard();
        assertEquals(true, umanoB.getPlayersDeath());
        assertEquals(0, umanoA.objectCardNumber());
    }

    /**
     * Test for method onSedativeCard: it verifies that the objectcard sedative is correctly used
     */
    @org.junit.Test
    public void testOnSedativeCard() {
        StateGame model = new StateGame();
        View view = new View();
        Controller controllo = new Controller(view, model);
        HumanPlayer umanoA = new HumanPlayer("a");
        ObjectCards carta = new ObjectCards("sedative");
        umanoA.setId(1);
        model.setPlayers(umanoA);
        model.setCurrentPlayer(umanoA.getID());
        umanoA.setObjectsOwned(carta);
        controllo.onSedativeCard();
        assertEquals(true, umanoA.getSedative());
        assertEquals(0, umanoA.objectCardNumber());
    }

    /**
     * Test for method onTeleportCard: it verifies that the objectcard teleport is correctly used
     */
    @org.junit.Test
    public void testOnTeleportCard() {
        StateGame model = new StateGame();
        View view = new View();
        Controller controllo = new Controller(view, model);
        HumanPlayer umanoA = new HumanPlayer("a");
        ObjectCards carta = new ObjectCards("teleport");
        Position posizione = new Position('l', 8);
        Position attuale = new Position('j', 11);
        List<Integer> lista = new ArrayList<Integer>();
        umanoA.setId(1);
        model.setPlayers(umanoA);
        model.setCurrentPlayer(umanoA.getID());
        umanoA.setObjectsOwned(carta);
        model.getMap().put(posizione, lista);
        lista.add(umanoA.getID());
        umanoA.setPosition(attuale);
        model.getMap().put(attuale, lista);
        controllo.onTeleportCard();
        assertEquals(posizione, umanoA.getPosition());
        assertEquals(false, umanoA.getTeleport());
        assertEquals(0, umanoA.objectCardNumber());
    }

    /**
     * Test for method onAdrenalineCard: it verifies that the objectcard adrenaline is correctly used
     */
    @org.junit.Test
    public void testOnAdrenalineCard() {
        StateGame model = new StateGame();
        View view = new View();
        Controller controllo = new Controller(view, model);
        HumanPlayer umanoA = new HumanPlayer("a");
        ObjectCards carta = new ObjectCards("adrenaline");
        umanoA.setId(1);
        model.setPlayers(umanoA);
        model.setCurrentPlayer(umanoA.getID());
        umanoA.setObjectsOwned(carta);
        controllo.onAdrenalineCard();
        assertEquals(true, umanoA.getAdrenaline());
        assertEquals(0, umanoA.objectCardNumber());
        Position corrente = new Position('j', 10);
        Position nuova = new Position('l', 11);
        List<Integer> lista = new ArrayList<Integer>();
        lista.add(umanoA.getID());
        umanoA.setPosition(corrente);
        model.getMap().put(corrente, lista);
        model.getMap().setPosition(umanoA, nuova);
        assertEquals(nuova, umanoA.getPosition());
        assertEquals(false, umanoA.getAdrenaline());
    }
}
