package it.polimi.ingsw.progetto;

import static org.junit.Assert.*;
import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;

import org.junit.Test;

public class PlayerTypeCardTest {

    /**
     * Test for methods isAlien and isHuman
     */
	@org.junit.Test
	public void testIsAlienIsHuman() {
		PlayerTypeCards tipoH = new PlayerTypeCards();
		PlayerTypeCards tipoA = new PlayerTypeCards();
		assertEquals(true, tipoA.isAlien());
		assertEquals(true, tipoH.isHuman());
	}
}
