package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;

import javax.swing.*;
import java.awt.event.*;

/**
 * Dialog for choice the object card to use
 */
public class UseObject extends JDialog {
    private JPanel contentPane;
    private JButton attaccoButton;
    private JButton luciButton;
    private JButton sedativoButton;
    private JButton teletrasportoButton;
    private JButton adrenalinaButton;
    private JButton annullaButton;
    private MouseListener attack;
    private MouseListener light;
    private MouseListener sedative;
    private MouseListener teleport;
    private MouseListener adrenaline;

    public UseObject(final Controller controller) {
        setContentPane(contentPane);
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setSize(350, 350);

        attaccoButton.setEnabled(false);
        luciButton.setEnabled(false);
        sedativoButton.setEnabled(false);
        teletrasportoButton.setEnabled(false);
        adrenalinaButton.setEnabled(false);
        annullaButton.setEnabled(true);

        annullaButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }
        });

        attack = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.onAttackCard();
                dispose();
            }
        };

        light = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.onSpotlightCard();
                dispose();
            }
        };

        sedative = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.onSedativeCard();
                dispose();
            }
        };

        teleport = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.onTeleportCard();
                dispose();
            }
        };

        adrenaline = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.onAdrenalineCard();
                dispose();
            }
        };
    }

    public void setAttaccoButton(boolean active){
        attaccoButton.setEnabled(active);
    }

    public void setAttaccoListener(boolean active){
        if (active) {
            attaccoButton.addMouseListener(attack);
        }else {
            attaccoButton.removeMouseListener(attack);
        }
    }

    public void setLuciButton(boolean active){
        luciButton.setEnabled(active);
    }

    public void setLuciListener(boolean active){
        if (active) {
            luciButton.addMouseListener(light);
        }else {
            luciButton.removeMouseListener(light);
        }
    }

    public void setSedativoButton(boolean active){
        sedativoButton.setEnabled(active);
    }

    public void setSedativoListener(boolean active){
        if (active) {
            sedativoButton.addMouseListener(sedative);
        }else {
            sedativoButton.removeMouseListener(sedative);
        }
    }

    public void setTeletrasportoButton(boolean active){
        teletrasportoButton.setEnabled(active);
    }

    public void setTeletrasportoListener(boolean active){
        if (active) {
            teletrasportoButton.addMouseListener(teleport);
        }else {
            teletrasportoButton.removeMouseListener(teleport);
        }
    }

    public void setAdrenalinaButton(boolean active){
        adrenalinaButton.setEnabled(active);
    }

    public void setAdrenalinaListener(boolean active){
        if (active) {
            adrenalinaButton.addMouseListener(adrenaline);
        }else {
            adrenalinaButton.removeMouseListener(adrenaline);
        }
    }
}
