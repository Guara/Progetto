package it.polimi.ingsw.progetto.model.decks;

import it.polimi.ingsw.progetto.model.cards.ObjectCards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for object deck
 *
 * @author Luigi Davide Greco, Daniele Guarascio
 */
public class ObjectDeck extends Decks {
    private List<ObjectCards> cardsArray = new ArrayList<ObjectCards>();
    private static String type = "attack";

    /**
     * Constructor
     */
    public ObjectDeck() {

    }

    /**
     * Method that creates a new object deck
     */
    public void createObjectDeck() {
        this.cardsArray = new ArrayList<ObjectCards>();
        for (max = 0; max < 12; max++) { //change card's type every two added
            if (max == 2) {
                type = "teleport";
            } else if (max == 4) {
                type = "sedative";
            } else if (max == 6) {
                type = "spotlight";
            } else if (max == 8) {
                type = "defense";
            } else if (max == 10) {
                type = "adrenaline";
            }
            cardsArray.add(ObjectCards.getObjectCard(type));
        }
        //mix the deck
        Collections.shuffle(cardsArray);
    }

    /**
     * Method that take and remove a card from the deck
     *
     * @return a card from the deck
     */
    public ObjectCards getCard() {
        ObjectCards carta = cardsArray.get(0);
        cardsArray.remove(0);
        return carta;
    }

    public void shuffleArray() {
        Collections.shuffle(cardsArray);
    }

    public void add(ObjectCards objectcard) {
        cardsArray.add(objectcard);
    }

    public List<ObjectCards> getArray() {
        return this.cardsArray;
    }
}
