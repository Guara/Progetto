package it.polimi.ingsw.progetto.model.players;

import it.polimi.ingsw.progetto.model.map.Board;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;

import java.util.List;


/**
 * Abstract class for the player that is extended by alienplayer and humanplayer
 *
 * @author Luigi Davide Greco, Daniele Guarascio
 */
public abstract class Players {
    protected static final char INITIALCOLUMN = 'l';
    protected static int COUNT = 1;
    protected String name;
    protected Position currentPosition;
    protected List<Position> historyPosition;
    protected List<ObjectCards> objectsOwned;
    protected boolean winner;
    protected boolean death;
    protected boolean defense;//it is activated when a player draws a card defense
    protected boolean adrenaline;
    protected boolean alienFeeding;
    protected int drawnCard; //array index object cards
    protected int id;
    protected boolean sedative;//It is activated when a player uses the card sedative
    protected boolean teleport;


    public String getName() {
        return this.name;
    }

    public Position getPosition() {
        return this.currentPosition;
    }

    public void setPosition(Position newposition) {
        this.currentPosition = newposition;
    }

    public void setHistoryPosition(Position posizione) {
        this.historyPosition.add(posizione);
    }

    public List<Position> getHistoryPosition() {
        return this.historyPosition;
    }

    /**
     * Method that sets death to true and the player is out of the game
     *
     * @param morto
     */
    public void setPlayersDeath(boolean morto) {
        this.death = morto;
    }

    public boolean getPlayersDeath() {
        return this.death;
    }

    public int getID() {
        return this.id;
    }

    /**
     * Method that set winner to true, then the player wins the game
     *
     * @param vinto
     */
    public void setPlayersWin(boolean vinto) {
        this.winner = vinto;
    }

    public boolean getPlayersWin() {
        return this.winner;
    }

    /**
     * Method that sets alienfeeding to true, then the alienplayer can do three sectors movement if he want
     *
     * @param mangiato
     */
    public void setAlienFeeding(boolean mangiato) {
        this.alienFeeding = mangiato;
    }

    public boolean getAlienFeeding() {
        return this.alienFeeding;
    }

    public void setObjectsOwned(ObjectCards carta) {
        this.objectsOwned.add(carta);
    }

    public List<ObjectCards> getObjectsOwned() {
        return this.objectsOwned;
    }

    /**
     * Method that removes the object card from the array when it's been used
     *
     * @param tipocarta
     * @controllo handles the case where the player has multiple cards of the same type
     */
    public void useObjectCard(int tipocarta) {
        boolean controllo = false;
        for (int i = 0; i < objectsOwned.size(); i++) {
            if (objectsOwned.get(i).getType() == tipocarta && !controllo) {
                objectsOwned.remove(i);
                controllo = true;
            }
        }
    }

    /**
     * Method that sets sedative to true and then the player doesn't have to draw a sector card for one round; it's used when the player use a sedative card
     *
     * @param sedativo
     */
    public void setSedative(boolean sedativo) {
        this.sedative = sedativo;
    }

    public boolean getSedative() {
        return this.sedative;
    }

    public Position setInitialPosition() {
        return new Position();
    }

    /**
     * Method that sets adrenaline to true and then the human player can do a two sectors movement for one turn; it's used when the player use an adrenaline card
     *
     * @param adrenalina
     */
    public void setAdrenaline(boolean adrenalina) {
        this.adrenaline = adrenalina;
    }

    public boolean getAdrenaline() {
        return this.adrenaline;
    }

    /**
     * Method that sets defense to true and then the human player can reject one attack
     *
     * @param attiva
     */
    public void setDefense(boolean attiva) {
        this.defense = attiva;
    }

    public void setPlayerOnMap() {
        Board.setNewPlayer(this);
    }

    public boolean getDefense() {
        return this.defense;
    }

    public Position getLastPosition() {
        int ultimaposizione = historyPosition.size();
        return historyPosition.get(ultimaposizione - 1);
    }

    public ObjectCards getObjectCard(int type) {
        ObjectCards card;
        for (int i = 0; i < objectsOwned.size(); i++) {
            card = objectsOwned.get(i);
            if (card.getType() == type) {
                return card;
            }
        }
        return null;
    }

    public int objectCardNumber() {
        return objectsOwned.size();
    }

    /**
     * Method useful for test
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method useful for Teleport card
     * implemented in Human Player
     *
     * @param teletrasporto
     */
    public void setTeleport(boolean teletrasporto) {
    }

    public boolean getTeleport() {
        return this.teleport;
    }
}