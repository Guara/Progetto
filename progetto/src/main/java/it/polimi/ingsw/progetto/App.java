package it.polimi.ingsw.progetto;

import it.polimi.ingsw.progetto.controller.Controller;
import it.polimi.ingsw.progetto.model.StateGame;
import it.polimi.ingsw.progetto.view.View;

/**
 * Main Application entry point
 */
public class App {
   private App(){
   }
   public static void main(String[] args) throws Exception {
      final StateGame game = new StateGame();
      View view = new View();
      final Controller controller = new Controller(view, game);

      view.createComponents(controller, game);

      // show the first form
      view.getInitFrame().setVisible(true);
   }
}
