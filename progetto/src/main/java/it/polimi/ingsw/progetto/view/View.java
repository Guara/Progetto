package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;
import it.polimi.ingsw.progetto.model.StateGame;
import it.polimi.ingsw.progetto.helpers.PlayerBuilder;
import it.polimi.ingsw.progetto.model.cards.EscapeHatchCards;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.players.AlienPlayer;
import it.polimi.ingsw.progetto.model.players.PlayerDescription;
import it.polimi.ingsw.progetto.model.players.Players;

import javax.swing.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static javax.swing.JOptionPane.*;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;

/**
 * Manage all frame
 */
public class View implements Observer {
    private static final int ATTACK = 0;
    private static final int TELEPORT = 1;
    private static final int SEDATIVE = 2;
    private static final int SPOTLIGHT = 3;
    private static final int DEFENSE = 4;
    private static final int ADRENALINE = 5;
    private JFrame initFrame;
    private UI gameFrame;
    private DrawTypeCard drawDialog;
    private GameType gameTypeDialog;
    private SpotlightPosition spotlightPositionDialog;
    private FakeNoisePosition fakePositionDialog;
    private UseObject useObjectDialog;
    private DiscardObject discardObjectDialog;
    private HistoryPosition historyPositionDialog;
    private PlayerDescription description;
    private Controller controller;
    private StateGame model;
    private static PlayerBuilder playerBuilder = new PlayerBuilder();
    private static int i;
    private List<ArrayList<String>> messagelist;

    public View() {
    }


    public void createComponents(final Controller controller, final StateGame model) {
        initFrame = new InitialFrame(controller);
        drawDialog = new DrawTypeCard(controller, model);
        gameTypeDialog = new GameType(controller, model);
        spotlightPositionDialog = new SpotlightPosition(controller);
        historyPositionDialog = new HistoryPosition();
        this.controller = controller;
        this.model = model;
        i = 1;
        model.setObserver(this);
    }

    public JFrame getInitFrame() {
        return initFrame;
    }

    public UI getGameFrame() {
        gameFrame = new UI(model, controller);
        return gameFrame;
    }

    public DrawTypeCard getDrawDialog() {
        return drawDialog;
    }

    public GameType getGameTypeDialog() {
        return gameTypeDialog;
    }

    public void setUseObjectDialog() {
        useObjectDialog = new UseObject(controller);
        Players player = model.getPlayerById(description.getId());
        List<ObjectCards> cardlist = player.getObjectsOwned();
        int count = 0;
        ObjectCards card;
        while (count < cardlist.size()) {
            card = cardlist.get(count);
            switch (card.getType()) {
                case ATTACK:
                    useObjectDialog.setAttaccoButton(true);
                    useObjectDialog.setAttaccoListener(true);
                    break;
                case TELEPORT:
                    useObjectDialog.setTeletrasportoButton(true);
                    useObjectDialog.setTeletrasportoListener(true);
                    break;
                case SEDATIVE:
                    useObjectDialog.setSedativoButton(true);
                    useObjectDialog.setSedativoListener(true);
                    break;
                case SPOTLIGHT:
                    useObjectDialog.setLuciButton(true);
                    useObjectDialog.setLuciListener(true);
                    break;
                case DEFENSE:
                    break;
                case ADRENALINE:
                    useObjectDialog.setAdrenalinaButton(true);
                    useObjectDialog.setAdrenalinaListener(true);
                    break;
                default:
                    break;
            }
            count++;
        }
        useObjectDialog.setVisible(true);
    }

    public void setHistoryButton(boolean active) {
        gameFrame.setHistoryButton(active);
    }

    public void setHistoryPositionDialog() {
        List<Position> history = model.getPlayerById(description.getId()).getHistoryPosition();
        String list = new String();
        Character column;
        Integer line;
        for (int i = 0; i < history.size(); i++) {
            column = history.get(i).getColumn();
            line = history.get(i).getLine();
            list = list.concat(column.toString()).concat(line.toString()).concat("\n");
        }
        Position currentposition = model.getPlayerById(model.getCurrentPlayer()).getPosition();
        column = currentposition.getColumn();
        line = currentposition.getLine();
        list = list.concat(column.toString()).concat(line.toString()).concat("\n");
        historyPositionDialog.printHistory(list);
        historyPositionDialog.setVisible(true);
    }

    public void askPlayersName() {
        String name;
        do {
            name = showInputDialog(initFrame, "Inserisci il nome", "Giocatore " + i, QUESTION_MESSAGE);
        } while (name != null && name.isEmpty());
        i++;
        playerBuilder.setName(name);
    }

    static PlayerBuilder getPlayerBuilder() {
        return playerBuilder;
    }

    public void askNumPlayers() {
        int players = 0;
        String input;
        while (players < 4 || players > 8) {
            input = showInputDialog(initFrame, "Inserisci il numero di giocatori", "Numero?", QUESTION_MESSAGE);
            try {
                players = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                players = 0;
            }
        }

        model.setPlayersNumber(players);
        model.setPlayerTypeDeck();
        messagelist = new ArrayList<ArrayList<String>>(players);
        for (int i = 0; i < players; i++) {
            messagelist.add(new ArrayList<String>());
        }
    }

    public void notifyPlayer() {
        gameFrame.printMessage("Tocca al giocatore " + description.getId() + ": " + description.getName());
    }

    public void getFirstPlayer() {
        description = model.getFirstPlayer();
        model.setCurrentPlayer(description.getId());
    }

    public void setCurrentPlayerDescription(PlayerDescription description) {
        this.description = description;
    }

    public PlayerDescription getCurrentPlayerDescription() {
        return description;
    }

    public void highlightsTextField() {
        gameFrame.highlightsTextField(description.getId() - 1);
    }

    public void highlightsDeathTextField(int i) {
        gameFrame.highlightsDeathTextField(i);
    }

    public void highlightsWinnerTextField(int i) {
        gameFrame.highlightsWinnerTextField(i);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof PlayerTypeCards) {
            PlayerTypeCards card = (PlayerTypeCards) arg;
            if (card.isAlien()) {
                showMessageDialog(gameFrame, "Sei un alieno");
            } else {
                showMessageDialog(gameFrame, "Sei un umano");
            }

        } else if (arg instanceof Integer) {
            if ((Integer) arg == 1) {
                showMessageDialog(gameFrame, "Movimento effettuato!");
                gameFrame.setMoveButton(false);
                if (model.getPlayerById(description.getId()) instanceof AlienPlayer) {
                    gameFrame.setAttackButton(true);
                }
            } else if ((Integer) arg == -1) {
                showMessageDialog(gameFrame, "Movimento non effettuato! Posizione non valida");
            } else if ((Integer) arg == 2) {
                List<PlayerDescription> playerDeath = model.getPlayersDeath();
                String[] names = new String[playerDeath.size()];
                String attacker = model.getAttackerName();
                for (int j = 0; j < playerDeath.size(); j++) {
                    names[j] = playerDeath.get(j).getName();
                }
                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < names.length; i++) {
                    strBuilder.append(names[i]).append(" ");
                }
                String column = Character.toString(model.getAttackposition().getColumn());
                String line = Integer.toString(model.getAttackposition().getLine());
                String attacksector = column + line;
                String newString = strBuilder.toString();
                messageForAllPlayers("I giocatori: " + newString + " sono morti per mano di " + attacker + " nel settore " + attacksector);
                messageToPlayer("Hai ucciso " + newString);
            } else if ((Integer) arg == -2) {
                String column = Character.toString(model.getAttackposition().getColumn());
                String line = Integer.toString(model.getAttackposition().getLine());
                String attacksector = column + line;
                String attacker = model.getAttackerName();
                messageForAllPlayers("Il giocatore " + attacker + " ha attaccato nel settore " + attacksector + " ma non ha ucciso nessuno!");
                messageToPlayer("Il tuo attacco non ha ucciso nessuno!");
            } else if ((Integer) arg == 3) {
                List<PlayerDescription> playerDeath = model.getPlayersDeath();
                List<PlayerDescription> playerDefense = model.getPlayerDefense();
                String[] deathnames = new String[playerDeath.size()];
                String[] defensenames = new String[playerDefense.size()];
                String attacker = model.getAttackerName();
                StringBuilder strBuilder = new StringBuilder();
                for (int j = 0; j < playerDeath.size(); j++) {
                    deathnames[j] = playerDeath.get(j).getName();
                }
                for (int j = 0; j < playerDefense.size(); j++) {
                    defensenames[j] = playerDefense.get(j).getName();
                }
                for (int i = 0; i < deathnames.length; i++) {
                    strBuilder.append(deathnames[i]).append(" ");
                }
                String newString = strBuilder.toString();
                for (int i = 0; i < defensenames.length; i++) {
                    strBuilder.append(defensenames[i]).append(" ");
                }
                String column = Character.toString(model.getAttackposition().getColumn());
                String line = Integer.toString(model.getAttackposition().getLine());
                String attacksector = column + line;
                String newString2 = strBuilder.toString();
                messageForAllPlayers("I giocatori: " + newString + " sono morti per mano di " + attacker + " nel settore " + attacksector + " i giocatori " + newString2 + " si sono difesi!");
                messageToPlayer("Hai ucciso " + newString + " I giocatori " + newString2 + " si sono difesi!");
            } else if ((Integer) arg == 4) {
                String name = getCurrentPlayerDescription().getName();
                Position noiseposition = model.getPlayerById(model.getCurrentPlayer()).getPosition();
                String column = Character.toString(noiseposition.getColumn());
                String line = Integer.toString(noiseposition.getLine());
                String noise = column + line;
                messageForAllPlayers("Il giocatore " + name + " dichiara di essere in " + noise);
                messageToPlayer("Hai dichiarato di essere in " + noise);
            } else if ((Integer) arg == 5) {
                setFakePositionDialog(true);
            } else if ((Integer) arg == 6) {
                List<PlayerDescription> playerDefense = model.getPlayerDefense();
                String[] defensenames = new String[playerDefense.size()];
                String attacker = model.getAttackerName();
                StringBuilder strBuilder = new StringBuilder();
                for (int j = 0; j < playerDefense.size(); j++) {
                    defensenames[j] = playerDefense.get(j).getName();
                }
                for (int i = 0; i < defensenames.length; i++) {
                    strBuilder.append(defensenames[i]).append(" ");
                }
                String column = Character.toString(model.getAttackposition().getColumn());
                String line = Integer.toString(model.getAttackposition().getLine());
                String attacksector = column + line;
                String newString2 = strBuilder.toString();
                messageForAllPlayers(attacker + " ha attaccato nel settore " + attacksector + " i giocatori " + newString2 + " si sono difesi! ");
                messageToPlayer("I giocatori " + newString2 + " si sono difesi!");
            }
        } else if (arg instanceof EscapeHatchCards) {
            String column = Character.toString(controller.getEscapehatchsectorbroken().getColumn());
            String line = Integer.toString(controller.getEscapehatchsectorbroken().getLine());
            String escapesector = column + line;
            if (((EscapeHatchCards) arg).isRed()) {
                messageToPlayer("Hai pescato una carta scialuppa rossa, non hai vinto!");
                messageForAllPlayers("Il giocatore " + description.getName() + " Ha pescato una carta scialuppa rossa, il settore " + escapesector + " è rotto!");
            } else {
                controller.onHumanWin();
                messageForAllPlayers("Il giocatore " + description.getName() + " Ha pescato una carta scialuppa verde e ha vinto! Il settore " + escapesector + " è rotto!");
            }
        }
    }

    public void saveMessageToPlayer(int id, String message) {
        messagelist.get(id - 1).add(message);
    }

    public void printMessageQuee(int id) {
        List<String> quee = messagelist.get(id - 1);
        while (!quee.isEmpty()) {
            messageToPlayer(quee.get(0));
            messagelist.get(id - 1).remove(0);
        }
    }

    public void messageForAllPlayers(String message) {
        int count = 1;
        while (count <= model.getPlayersNumber()) {
            if (count == description.getId()) {
                count++;
                continue;
            }
            saveMessageToPlayer(count, message);
            count++;
        }
    }

    public void setUIButtons() {
        Players currentPlayer = model.getPlayerById(description.getId());
        gameFrame.setHistoryButton(true);
        if (currentPlayer instanceof AlienPlayer) {
            gameFrame.setMoveButton(true);
            gameFrame.setUseObjectButton(false);
            gameFrame.addUseObjectButton(false);
            gameFrame.addAttackButton(true);
            gameFrame.setAttackButton(false);
        } else {
            gameFrame.setMoveButton(true);
            gameFrame.addAttackButton(false);
            gameFrame.setAttackButton(false);
            gameFrame.addUseObjectButton(true);
            if (currentPlayer.objectCardNumber() > 0) {
                gameFrame.setUseObjectButton(true);
            } else {
                gameFrame.setUseObjectButton(false);
            }
        }
    }

    public void setSpotlightPositionDialog(boolean active) {
        if (active) {
            spotlightPositionDialog.pack();
            spotlightPositionDialog.setEnabled(active);
            spotlightPositionDialog.setVisible(active);
        } else {
            spotlightPositionDialog.setEnabled(active);
            spotlightPositionDialog.setVisible(active);
        }
    }

    public void setEndturnButton(boolean active) {
        gameFrame.setEndturnButton(active);
    }

    public void messageToPlayer(String message) {
        showMessageDialog(gameFrame, message, "Messaggio", INFORMATION_MESSAGE);
    }

    public void revelPlayersOnSector(List<Integer> playersonsector, Position position) {
        if (playersonsector == null) {
            showMessageDialog(gameFrame, "Nessun Giocatore in Posizione " + position.getColumn() + position.getLine(), "Effetto carta Luci", INFORMATION_MESSAGE);
            return;
        }
        PlayerDescription playerDescription;
        String[] playerOnSector = new String[playersonsector.size()];
        for (int j = 0; j < playersonsector.size(); j++) {
            playerDescription = model.getPlayerDescripionByID(playersonsector.get(j));
            playerOnSector[j] = playerDescription.getName();
        }
        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < playerOnSector.length; i++) {
            strBuilder.append(playerOnSector[i]).append(" ");
        }
        String newString = strBuilder.toString();
        showMessageDialog(gameFrame, "Giocatori in Posizione " + position.getColumn() + position.getLine() + ": " + newString, "Effetto carta Luci", INFORMATION_MESSAGE);
    }

    public void setObjectButton(boolean active) {
        gameFrame.setUseObjectButton(active);
    }

    public void youWin() {
        showMessageDialog(gameFrame, "Complimenti Hai Vinto!", "Hai Vinto!", INFORMATION_MESSAGE);
    }

    public void useOrDiscard() {
        UseOrDiscard useOrDiscardDialog = new UseOrDiscard(controller);
        useOrDiscardDialog.setEnabled(true);
        useOrDiscardDialog.setVisible(true);
    }

    public void setAttackButton() {
        gameFrame.setAttackButton(false);
    }

    public void setFakePositionDialog(boolean active) {
        if (active) {
            fakePositionDialog = new FakeNoisePosition(controller);
            fakePositionDialog.pack();
            fakePositionDialog.setEnabled(active);
            fakePositionDialog.setVisible(active);
        } else {
            fakePositionDialog.dispose();
        }
    }

    public void resetTextField() {
        gameFrame.resetTextField();
    }

    public void notifyFakePosition(Position fakeposition) {
        String name = getCurrentPlayerDescription().getName();
        String column = Character.toString(fakeposition.getColumn());
        String line = Integer.toString(fakeposition.getLine());
        String noise = column + line;
        messageForAllPlayers("Il giocatore " + name + " dichiara di essere in " + noise);
        messageToPlayer("Hai dichiarato di essere in posizione " + noise);
    }

    public void removehighlightsTextField(PlayerDescription description) {
        gameFrame.removehighlightsTextField(description.getId() - 1);
    }

    public void setDiscarObjectDialog() {
        discardObjectDialog = new DiscardObject(controller);
        Players player = model.getPlayerById(description.getId());
        List<ObjectCards> cardlist = player.getObjectsOwned();
        int count = 0;
        ObjectCards card;
        while (count < cardlist.size()) {
            card = cardlist.get(count);
            switch (card.getType()) {
                case ATTACK:
                    discardObjectDialog.setAttaccoButton(true);
                    discardObjectDialog.setAttaccoListener(true);
                    break;
                case TELEPORT:
                    discardObjectDialog.setTeletrasportoButton(true);
                    discardObjectDialog.setTeletrasportoListener(true);
                    break;
                case SEDATIVE:
                    discardObjectDialog.setSedativoButton(true);
                    discardObjectDialog.setSedativoListener(true);
                    break;
                case SPOTLIGHT:
                    discardObjectDialog.setLuciButton(true);
                    discardObjectDialog.setLuciListener(true);
                    break;
                case DEFENSE:
                    discardObjectDialog.setDifesaButton(true);
                    discardObjectDialog.setDifesaListener(true);
                    break;
                case ADRENALINE:
                    discardObjectDialog.setAdrenalinaButton(true);
                    discardObjectDialog.setAdrenalinaListener(true);
                    break;
                default:
                    break;
            }
            count++;
        }
        discardObjectDialog.setVisible(true);
        discardObjectDialog.dispose();
    }
}
