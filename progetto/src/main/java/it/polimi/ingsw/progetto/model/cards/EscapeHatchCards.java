package it.polimi.ingsw.progetto.model.cards;

/**
 * class for escape hatch cards
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class EscapeHatchCards implements Cards {
   private final boolean type;
   
   /**
    * Constructor
    * @param color red disabled the sector, green enabled it
    */
   public EscapeHatchCards(String color) {
      type = "green".equals(color);
   }

   public boolean isGreen() {
      return type;
   }
   
   public boolean isRed() {
      return !type;
   }
   
}
