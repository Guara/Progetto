package it.polimi.ingsw.progetto.model.map;

/**
 * Class for positions in the map
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class Position {
  
   private char column;
   private int line;
   private SectorsType settore;
   private boolean escapehatchbroken;

    /**
    * Contructor for getInitialPosition in players class
    */
   public Position() {
      
   }
   
   /**
    * Contructor of the position
    * @param column
    * @param line
    */
   public Position(char column, int line) {
      this.column = column;
      this.line = line;
      this.settore = null;
      this.escapehatchbroken = false;
   }
   
   /**
    * Method that change the position
    * @param column
    * @param line
    */
   public void setPosition(int column, int line) {
      this.column = (char) column;
      this.line = line;
   }
   
   public void setLine (int line){
      this.line = line;
   }
   
   public void setColumn (char column){
      this.column = column;
   }
   
   public int getLine() {
      return this.line;
   }

   public char getColumn() {
      return this.column;
   }
   
   public void setSectorType(SectorsType sector) {
      this.settore = sector;
   }
   
   public SectorsType getSectorType() {
      return this.settore;
   }
   
   /**
    * Method to set escapehatchbroken to true or false, dipends from the escapehatchcard that is drawn
    * @param rotto
    */
   public void setEscapeHatchBroken(boolean rotto) {
      this.escapehatchbroken = rotto;
   }
   
   /**
    * 
    * @return true if the escapehatchsector is disabled
    */
   public boolean getEscapeHatchBroken() {
      return this.escapehatchbroken;
   }
}
