package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * This dialog allows to choose whether to use or discard an object card
 * when the player own 4 object cards
 */
public class UseOrDiscard extends JDialog {
   private JPanel contentPane;
   private JButton usaButton;
   private JButton scartaButton;

   public UseOrDiscard(final Controller controller) {
      setContentPane(contentPane);
      setModal(true);
      setLocationByPlatform(true);
       setSize(300, 300);
      usaButton.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            controller.onUseObjectCard();
             dispose();
         }
      });

      scartaButton.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent e) {
            controller.onDiscardObjectCard();
             dispose();
         }
      });
   }
}
