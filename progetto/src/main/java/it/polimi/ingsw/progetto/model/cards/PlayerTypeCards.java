package it.polimi.ingsw.progetto.model.cards;

/**
 * Class for player type cards
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class PlayerTypeCards implements Cards {
   private static final boolean ALIEN = false;
   private static final boolean HUMAN = true;
   private static int count = 0;
   private boolean type;

   /**
    * Constructor
    */
   public PlayerTypeCards() {
      if (count % 2 == 0) {
         this.type = ALIEN;
      } else {
         this.type = HUMAN;
      }
      count++;
   }
   
   /**
    * 
    * @return a new playertypecards object
    */
   public static PlayerTypeCards addPlayerTypeCard() {
      return new PlayerTypeCards();
   }

   public boolean isAlien() {
      return type == ALIEN;
   }

   public boolean isHuman() {
      return type == HUMAN;
   }
}
