package it.polimi.ingsw.progetto.model.decks;

import it.polimi.ingsw.progetto.model.cards.SectorCards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for sector deck
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class SectorDeck extends Decks {
   private static final int DIMENSION = 25;
   private static final int SILENCE = 5;
   private static final int NOISE = 10;
   List<SectorCards> cardsarray = new ArrayList<SectorCards> (DIMENSION);
   private static String type = "silence";
   
   /**
    * Constructor
    */
   public SectorDeck() {
      this.cardsarray = new ArrayList<SectorCards> (DIMENSION);
   }
   
   /**
    * Method that creates a new sector deck with twenty-five sector cards
    * @return
    */
   public List<SectorCards> setSectorDeck(){
      //insert silence cards
      for(max=0; max<SILENCE; max++){
         cardsarray.add(SectorCards.getSectorCard(type));
      }
      //insert noise in your sector cards
      type = "truenoise";
      SectorCards carta = new SectorCards(type);
      for(max=0; max<NOISE; max++){
         if (max == 6){
             carta = new SectorCards(type);
            carta.setObjectIcon(true);
         }
         cardsarray.add(carta);
      }
      //insert noise in chosen sector cards
      type = "fakenoise";
      carta = new SectorCards(type);
      for(max=0; max<NOISE; max++){
         if (max == 6){
             carta = new SectorCards(type);
            carta.setObjectIcon(true);
         }
         cardsarray.add(carta);
      }
      //mix the deck
      Collections.shuffle(cardsarray);
      return cardsarray;
      }

   /**
    * Method that take and remove a card from the deck
    * @return a card from the deck
    */
   public SectorCards getCard() {
       SectorCards card = null;
       if (!cardsarray.isEmpty()) {
           card = cardsarray.get(0);
           cardsarray.remove(0);
       }
           return card;
   }
   
   public void addSectorCard(SectorCards cartasettore) {
      cardsarray.add(cartasettore);
   }
}