package it.polimi.ingsw.progetto.view;


import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;


import javax.swing.*;
import javax.swing.border.EmptyBorder;

import it.polimi.ingsw.progetto.controller.Controller;
import it.polimi.ingsw.progetto.model.StateGame;
import it.polimi.ingsw.progetto.model.map.Position;


/**
 * Class of the Game Frame implements several button for play the game
 * Methods implemented in this class are necessary for modify the game frame
 */
public class UI extends JFrame {
    private static final int DEFENSE = 4;
    private static final long serialVersionUID = 1L;
    private static JTextField colonna;
    private static JTextField riga;
    private List<JTextField> player;
    private StateGame model;
    private Controller controller;
    private JPanel contentPane;
    private JButton move;
    private JButton attack;
    private JButton useobjectcard;
    private JButton endturn;
    private JLabel background;
    private JLabel playerslist;
    private JLabel label1;
    private JLabel label2;
    private JButton historyPosition;
    private MouseAdapter movelistener;
    private ActionListener cardlistener;
    private MouseAdapter attacklistener;
    private MouseAdapter endturnlistener;
    private MouseAdapter historylistener;

    public UI(final StateGame model, final Controller controller) {
        this.model = model;
        this.controller = controller;
        createUI();
    }

    public void createUI() {
        //codice frame e pannello
        setResizable(false);
        setTitle("ESCAPE FROM ALIENS IN OUTER SPACE");
        setSize(1280, 720);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        colonna = new JTextField("Colonna");
        riga = new JTextField("Riga");
        colonna.setBounds(220, 30, 60, 20);
        riga.setBounds(220, 60, 60, 20);
        move = new JButton("Muoviti!");
        move.setBounds(50, 30, 150, 50);
        contentPane.add(colonna);
        contentPane.add(riga);
        contentPane.add(move);
        move.setEnabled(true);
        movelistener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String columntext = colonna.getText().toLowerCase();
                char column = 'Z';
                int line = 99;
                // Require the String to have exactly one character.
                if (columntext.length() != 1) {
                    // Error state.
                } else {
                    column = columntext.charAt(0);
                }
                String linetext = riga.getText();
                if (linetext.length() < 1 && linetext.length() > 2) {
                    // Error state.
                } else {
                    line = Integer.parseInt(linetext);
                }
                if (line != 99 && column != 'Z') {
                    controller.onNewPosition(setNewPosition(line, column));
                }//imposto nuova posizione
            }
        };
        attack = new JButton("Attacca!");
        attack.setBounds(824, 30, 150, 50);
        attack.setEnabled(false);
        attacklistener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.onAttack();
            }
        };
        attack.addMouseListener(attacklistener);
        useobjectcard = new JButton("Usa carta oggetto!");
        useobjectcard.setBounds(824, 30, 150, 50);
        contentPane.add(useobjectcard);
        useobjectcard.setEnabled(false);
        useobjectcard.setVisible(false);
        cardlistener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                controller.onUseObjectCard();
            }
        };
        endturn = new JButton("Passa Turno");
        contentPane.add(endturn);
        endturn.setBounds(425, 30, 150, 50);
        endturn.setVisible(true);
        endturn.setEnabled(false);
        endturnlistener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                controller.onEndTurn();
            }
        };

        //codice sfondo
        background = new JLabel();
        background.setIcon(new ImageIcon(InitialFrame.class.getResource("/images/board.jpg")));
        background.setBounds(0, 137, 1000, 560);
        contentPane.add(background);

        //codice label1
        label1 = new JLabel();
        label1.setIcon(new ImageIcon(InitialFrame.class.getResource("/images/label1.jpg")));
        label1.setBounds(0, -30, 500, 200);
        contentPane.add(label1);

        //codice label2
        label2 = new JLabel();
        label2.setIcon(new ImageIcon(InitialFrame.class.getResource("/images/label2.jpg")));
        label2.setBounds(500, -32, 500, 200);
        contentPane.add(label2);

        //codice lista giocatori
        int h = 30;
        int playersNumber = model.getPlayersNumber();
        player = new ArrayList<JTextField>(playersNumber);
        for (int i = 0; i < playersNumber; i++) {
            player.add(i, new JTextField(model.getName().get(i)));
            JTextField field = player.get(i);
            field.setBounds(1030, h, 212, 19);
            field.setEnabled(false);
            contentPane.add(field);
            h = h + 55;
        }
        historyPosition = new JButton("Lista movimenti");
        historyPosition.setBounds(1005, 620, 150, 50);
        contentPane.add(historyPosition);
        historyPosition.setVisible(true);
        historyPosition.setEnabled(false);
        historylistener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.onHistoryPosition();
            }
        };

        historyPosition.addMouseListener(historylistener);
        playerslist = new JLabel();
        playerslist.setBounds(1000, 0, 280, 720);
        playerslist.setIcon(new ImageIcon(InitialFrame.class.getResource("/images/LabelBackGround.jpg")));
        contentPane.add(playerslist);
    }

    @Override
    public JPanel getContentPane() {
        return contentPane;
    }

    public void printMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    public void addAttackButton(boolean add) {
        if (add) {
            contentPane.add(attack);
        } else {
            contentPane.remove(attack);
        }
    }

    public void setEndturnButton(boolean active) {
        if (active) {
            endturn.setEnabled(active);
            endturn.addMouseListener(endturnlistener);
        } else {
            endturn.setEnabled(active);
            endturn.removeMouseListener(endturnlistener);
        }
    }

    public void addUseObjectButton(boolean add) {
        if (add) {
            useobjectcard.setVisible(true);
        } else {
            useobjectcard.setVisible(false);
        }
    }

    public void setUseObjectButton(boolean active) {
        if (active) {
            if (model.getPlayerById(model.getCurrentPlayer()).getObjectsOwned().size() == 1) {
                if (model.getPlayerById(model.getCurrentPlayer()).getObjectsOwned().get(0).getType() == DEFENSE) {
                    useobjectcard.setEnabled(!active);
                    useobjectcard.removeActionListener(cardlistener);
                    return;
                }
            }
            if (!model.getPlayerById(model.getCurrentPlayer()).getObjectsOwned().isEmpty()) {

                useobjectcard.setEnabled(active);
                useobjectcard.addActionListener(cardlistener);
            } else {
                useobjectcard.setEnabled(!active);
                useobjectcard.removeActionListener(cardlistener);
            }
        } else {
            useobjectcard.setEnabled(active);
            useobjectcard.removeActionListener(cardlistener);
        }
    }

    public Position setNewPosition(int line, char column) {
        return new Position(column, line);
    }

    public void setAttackButton(boolean active) {
        if (active) {
            attack.setEnabled(active);
            attack.addMouseListener(attacklistener);
        } else {
            attack.setEnabled(active);
            attack.removeMouseListener(attacklistener);
        }
    }

    public void setMoveButton(boolean active) {
        if (active) {
            move.setEnabled(active);
            move.addMouseListener(movelistener);
        } else {
            move.setEnabled(active);
            move.removeMouseListener(movelistener);
        }
    }

    public void highlightsTextField(int i) {
        JTextField field = player.get(i);
        field.setBackground(Color.DARK_GRAY);
    }

    public void highlightsDeathTextField(int i) {
        JTextField field = player.get(i);
        field.setBackground(Color.RED);
    }

    public void highlightsWinnerTextField(int i) {
        JTextField field = player.get(i);
        field.setBackground(Color.GREEN);
    }

    public void removehighlightsTextField(int i) {
        player.get(i).setBackground(Color.WHITE);
    }

    public void setHistoryButton(boolean active) {
        if (active) {
            historyPosition.setEnabled(active);
        } else {
            historyPosition.setEnabled(false);
        }
    }

    public void resetTextField() {
        colonna.setText("Colonna");
        riga.setText("Riga");
    }
}

