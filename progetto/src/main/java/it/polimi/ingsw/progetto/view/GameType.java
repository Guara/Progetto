package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;
import it.polimi.ingsw.progetto.model.StateGame;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * A dialog for choice the type of the gaming
 */
public class GameType extends JDialog {
    private JPanel contentPane;
    private JButton offlineButton;
    Controller controller;

    public GameType(final Controller controller, final StateGame model) {
        this.controller = controller;
        setContentPane(contentPane);
        setModal(true);
        setLocationByPlatform(true);
        getRootPane().setDefaultButton(offlineButton);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        offlineButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                setEnabled(false);
                setVisible(false);
                controller.offlineGame(model);
            }
        });
    }
}
