package it.polimi.ingsw.progetto.model.players;

import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.map.SectorsType;

import java.util.ArrayList;

/**
 * Class for alien player that extends players
 *
 * @author Luigi Davide Greco, Daniele Guarascio
 */
public class AlienPlayer extends Players {

    private static final int INITIALROWALIEN = 6;

    /**
     * Constructor
     *
     * @param name
     */
    public AlienPlayer(String name) {
        id = COUNT;
        this.objectsOwned = new ArrayList<ObjectCards>();
        this.name = name;
        this.historyPosition = new ArrayList<Position>();
        this.winner = false;
        this.death = false;
        this.alienFeeding = false;
        this.drawnCard = 0;
        this.sedative = false;
        this.defense = false;
        COUNT++;
        currentPosition = setInitialPosition();
        this.teleport = false;
    }

    /**
     * Method that puts the new alien player in the map on the express sector
     */
    @Override
    public Position setInitialPosition() {
        Position position = new Position(INITIALCOLUMN, INITIALROWALIEN);
        position.setSectorType(SectorsType.ALIEN);
        return position;
    }

    /**
     * This override is empty because an alien player can't use a object card
     *
     * @param teletrasporto
     */
    @Override
    public void setTeleport(boolean teletrasporto) {
    }
}

