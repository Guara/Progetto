package it.polimi.ingsw.progetto.view;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HistoryPosition extends JDialog {
   private JPanel contentPane;
   private JButton buttonOK;
   private JTextArea textArea1;

   public HistoryPosition() {
      textArea1.setEnabled(false);
      setContentPane(contentPane);
      setModal(true);
      getRootPane().setDefaultButton(buttonOK);
      buttonOK.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            dispose();
         }
      });
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      setLocationByPlatform(true);
      setSize(200, 300);
   }

   public void printHistory(String history) {
      textArea1.setText(null);
      textArea1.append(history);
   }
}
