package it.polimi.ingsw.progetto.model;

/**
 * Enumeration that describes the kind of client we have
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public enum GameClient {
   SERVER,
   CLIENT
}


