package it.polimi.ingsw.progetto.model.cards;

/**
 * Class for object cards
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class ObjectCards implements Cards {
   private int type;
   private static final int ATTACK = 0;
   private static final int TELEPORT = 1;
   private static final int SEDATIVE = 2;
   private static final int SPOTLIGHT = 3;
   private static final int DEFENSE = 4;
   private static final int ADRENALINE = 5;

   /**
    * Constructor
    * @param tipo defines the type of the card
    */
   public ObjectCards(String tipo){
      if (tipo == "attack"){
         type = ATTACK;
      }
      if (tipo == "teleport"){
         type = TELEPORT;
      }
      if (tipo == "sedative"){
         type = SEDATIVE;
      }
      if (tipo == "spotlight"){
         type = SPOTLIGHT;
      }
      if (tipo == "defense"){
         type = DEFENSE;
      }
      if (tipo == "adrenaline"){
         type = ADRENALINE;
      }
   }
   
   public ObjectCards (int tipocarta) {
      type = tipocarta;
   }
   
   /**
    * 
    * @param type
    * @return a new objectcards object
    */
   public static ObjectCards getObjectCard(String type){
      return new ObjectCards(type);
   }
   
   public static ObjectCards getObjectCard(int tipocarta) {
      return new ObjectCards(tipocarta);
   }
   
   public int getType() {
      return this.type;
   }

   public String getTypeString(int type){
      switch (type){
         case ATTACK:
            return  "Attacco";
         case TELEPORT:
            return  "Teletrasporto";
         case SEDATIVE:
            return  "Sedativo";
         case SPOTLIGHT:
            return  "Luci";
         case DEFENSE:
            return  "Difesa";
         case ADRENALINE:
            return  "Adrenalina";
         default:
            return "";
      }
   }
   
}
