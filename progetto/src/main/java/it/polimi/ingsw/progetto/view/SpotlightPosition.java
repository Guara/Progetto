package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;
import it.polimi.ingsw.progetto.model.map.Position;

import javax.swing.*;
import java.awt.event.*;

/**
 * This dialog is useful for inserting the position where use the Spotlight card
 */
public class SpotlightPosition extends JDialog {
   private JPanel contentPane;
   private JButton buttonOK;
   private JTextField colonnaTextField;
   private JTextField rigaTextField;
    private Position positionspotlight;

   public SpotlightPosition(final Controller controller) {
      setContentPane(contentPane);
      setModal(true);
      getRootPane().setDefaultButton(buttonOK);
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      setLocationByPlatform(true);

      buttonOK.addActionListener(new ActionListener() {
          @Override
         public void actionPerformed(ActionEvent e) {
            String columntext = colonnaTextField.getText().toLowerCase();
            char column = 'Z';
            int line = 99;
            // Require the String to have exactly one character.
            if (columntext.length() != 1) {
               // Error state.
            } else {
               column = columntext.charAt(0);
            }
            String linetext = rigaTextField.getText();
            if (linetext.length() < 1 && linetext.length() > 2) {
               // Error state.
            } else {
               line = Integer.parseInt(linetext);
            }
            if (line != 99 && column != 'Z') {
                positionspotlight = new Position(column, line);
            }
              controller.onSpotlightPosition(positionspotlight);
             dispose();
         }
      });
   }
}
