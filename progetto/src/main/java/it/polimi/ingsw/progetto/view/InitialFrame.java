package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * The initial frame of the game
 * show a splash screen and a button for start game
 */
public class InitialFrame extends JFrame {
   private static final long serialVersionUID = 1L;
   private String name;

   /**
    * create the initial frame
    * @param controller the controller
    */
   public InitialFrame(final Controller controller) {
      JPanel contentPane;
      JLabel background;
      JButton newGame;


      setResizable(false);
      //codice frame e pannello
      setTitle("ESCAPE FROM ALIENS IN OUTER SPACE");
      setSize(1280, 720);
      setLocationRelativeTo(null);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      contentPane = new JPanel();
      contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
      setContentPane(contentPane);
      contentPane.setLayout(null);

      //bottone NewGame
      newGame = new JButton("New Game");
      newGame.setBounds(565, 600, 150, 50);
      newGame.setEnabled(false);
      newGame.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseClicked(MouseEvent event) {
            controller.onNewGame();
         }
      });

      //codice sfondo
      background = new JLabel();
      background.setIcon(new ImageIcon(InitialFrame.class.getResource("/images/InitialFrame.jpg")));
      background.setBounds(0, 0, 1280, 720);

      // add childs to contentPane
      contentPane.add(newGame);
      contentPane.add(background);
   }

   @Override
   public synchronized void setName(String name) {
      this.name = name;
   }

   @Override
   public String getName() {
      return name;
   }
}