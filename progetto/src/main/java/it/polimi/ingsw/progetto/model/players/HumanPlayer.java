package it.polimi.ingsw.progetto.model.players;

import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.map.SectorsType;

import java.util.ArrayList;

/**
 * Class for human player that extends players
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class HumanPlayer extends Players {

   private static final int INITIALROWHUMAN = 8;

   /**
    * Constructor
    * @param name
    */
   public HumanPlayer(String name) {
      id = COUNT;
      this.objectsOwned = new ArrayList<ObjectCards>();
      this.name = name;
      this.historyPosition = new ArrayList<Position>();
      this.winner = false;
      this.death = false;
      this.alienFeeding = false;
      this.drawnCard = 0;
      this.sedative = false;
      this.defense = false;
      COUNT++;
      currentPosition = setInitialPosition();
       this.teleport = false;
   }

   /**
    * Method that puts the new human player in the map on the express sector
    */
   @Override
   public Position setInitialPosition() {
      Position position = new Position(INITIALCOLUMN, INITIALROWHUMAN);
      position.setSectorType(SectorsType.HUMAN);
      return position;
   }

    /**
     * Override of the method declared in Players interface
     * useful for Teleport card
      * @param teletrasporto
     */
   @Override
   public void setTeleport(boolean teletrasporto){
      this.teleport = teletrasporto;
   }
}
