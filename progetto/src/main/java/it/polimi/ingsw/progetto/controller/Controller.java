package it.polimi.ingsw.progetto.controller;

import it.polimi.ingsw.progetto.model.StateGame;
import it.polimi.ingsw.progetto.model.cards.EscapeHatchCards;
import it.polimi.ingsw.progetto.model.cards.ObjectCards;
import it.polimi.ingsw.progetto.model.cards.SectorCards;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.map.SectorsType;
import it.polimi.ingsw.progetto.model.players.HumanPlayer;
import it.polimi.ingsw.progetto.model.players.PlayerDescription;
import it.polimi.ingsw.progetto.model.players.Players;
import it.polimi.ingsw.progetto.view.View;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Controller receive the view and the model,
 * It handles events of view and the order of the game
 */
public class Controller {

    private static final int ATTACK = 0;
    private static final int TELEPORT = 1;
    private static final int SEDATIVE = 2;
    private static final int SPOTLIGHT = 3;
    private static final int DEFENSE = 4;
    private static final int ADRENALINE = 5;
    private static final int MAXTURNS = 39;
    private final View view;
    private final StateGame model;
    private final ExecutorService executorService;
    private PlayerDescription description;
    private Position escapehatchsectorbroken;

    public Controller(View view, StateGame model) {
        this.view = view;
        this.model = model;

        ThreadFactory threadFactory = new ThreadFactory() {
            private AtomicInteger uid = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "controllerJob-" + (uid.getAndIncrement()));
            }
        };


        this.executorService = Executors.newCachedThreadPool(threadFactory);
    }

    public void onInitDone() {


        JFrame frame = view.getGameFrame();

        frame.setEnabled(true);
        frame.setVisible(true);

        beginGame();
    }

    public void onNewGame() {
        JFrame frame = view.getInitFrame();

        frame.setEnabled(false);
        frame.setVisible(false);

        JDialog gameTypeDialog = view.getGameTypeDialog();

        gameTypeDialog.pack();
        gameTypeDialog.setEnabled(true);
        gameTypeDialog.setVisible(true);
    }

    public void onNameEntered() {
        JDialog drawDialog = view.getDrawDialog();

        drawDialog.pack();
        drawDialog.setEnabled(true);
        drawDialog.setVisible(true);
    }

    public void onCardDrawn() {
        JDialog drawDialog = view.getDrawDialog();

        drawDialog.setEnabled(false);
        drawDialog.setVisible(false);

    }

    public void onNumberEntered() {
        view.askPlayersName();
    }

    public void askNumPlyers() {
        view.askNumPlayers();
    }

    public void offlineGame(final StateGame model) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                askNumPlyers();
                int playersnumber = model.getPlayersNumber();
                for (int i = 0; i < playersnumber; i++) {
                    onNumberEntered();
                    onNameEntered();
                }
                onInitDone();
            }
        });
    }

    public void beginGame() {
        view.getFirstPlayer();
        view.notifyPlayer();
        view.setUIButtons();
        view.highlightsTextField();
        model.incrementTurnnumber();
    }

    /**
     * Method called by the movelistener when the player clicks on the move button after setting the position
     *
     * @param position setted by the player, it's where he wants to move himself
     */
    public void onNewPosition(Position position) {
        description = view.getCurrentPlayerDescription();
        Players player = model.getPlayerById(description.getId());
        Position currentposition = player.getPosition();
        if (player instanceof HumanPlayer) {
            model.setPosition(player, position);
            if (model.getChangePositionEffect()) {
                view.resetTextField();
                player.setHistoryPosition(currentposition);
                if (player.getPosition().getSectorType() == SectorsType.DANGEROUS) {
                    if (!player.getSedative()) {
                        SectorCards carta = model.drawSectorCard();
                        if (carta == null) {
                            model.setNewSectorDeck();
                            carta = model.drawSectorCard();
                        }
                        view.messageToPlayer("Hai Pescato la carta " + carta.getTypeString());
                        if (carta.getObjectIcon()) {
                            view.messageToPlayer("La carta che hai pescato ha l'icona carta oggetto, verrà automaticamente pescata una carta");
                            ObjectCards cartaoggetto = model.drawObjectCard();
                            if (cartaoggetto == null) {
                                model.resetObjectDeck();
                                cartaoggetto = model.drawObjectCard();
                            }
                            player.setObjectsOwned(cartaoggetto);
                            view.messageToPlayer("Hai pescato la carta: " + cartaoggetto.getTypeString(cartaoggetto.getType()));
                            if (model.limitObjectsOwned(player)) {
                                view.messageToPlayer("Hai raggiunto il limite di carte oggetto");
                                view.useOrDiscard();
                            }
                            List<ObjectCards> objects = player.getObjectsOwned();
                            ObjectCards card;
                            for (int i = 0; i < objects.size(); i++) {
                                card = objects.get(i);
                                if (card.getType() == DEFENSE) {
                                    player.setDefense(true);
                                }
                            }
                        }
                        model.sectorEffect(carta.getType());
                    } else {
                        view.messageToPlayer("Hai usato la carta sedativo non devi pescare una carta settore!");
                        player.setSedative(false);
                    }
                } else if (player.getPosition().getSectorType() == SectorsType.ESCAPEHATCH) {
                    EscapeHatchCards carta = model.drawEscapeHatchCard();
                    escapehatchsectorbroken = player.getPosition();
                    model.escapeHatchEffect(carta);
                    if (!player.getPosition().getEscapeHatchBroken()) {
                        player.getPosition().setEscapeHatchBroken(true);
                        player.setPlayersWin(true);
                        view.youWin();
                    } else {
                        Position lastposition = player.getLastPosition();
                        model.getMap().setPosition(player, lastposition);
                    }
                }
            }
            if (!(player.getObjectsOwned().size() == 1 && player.getObjectsOwned().get(0).getType() == DEFENSE)) {
                view.setObjectButton(true);
            }
        } else {
            model.setPosition(player, position);
            view.resetTextField();
            if (model.getChangePositionEffect()) {
                player.setHistoryPosition(currentposition);
                if (player.getPosition().getSectorType() == SectorsType.DANGEROUS) {
                    SectorCards carta = model.drawSectorCard();
                    if (carta == null) {
                        model.setNewSectorDeck();
                        carta = model.drawSectorCard();
                    }
                    view.messageToPlayer("Hai Pescato la carta " + carta.getTypeString());
                    if (carta.getObjectIcon()) {
                        view.messageToPlayer("La carta che hai pescato ha l'icona carta oggetto, verrà automaticamente pescata una carta");
                        ObjectCards cartaoggetto = model.drawObjectCard();
                        if (cartaoggetto == null) {
                            model.resetObjectDeck();
                            cartaoggetto = model.drawObjectCard();
                        }
                        player.setObjectsOwned(cartaoggetto);
                        view.messageToPlayer("Hai pescato la carta: " + cartaoggetto.getTypeString(cartaoggetto.getType()));
                        if (model.limitObjectsOwned(player)) {
                            view.messageToPlayer("Hai raggiunto il limite di carte oggetto");
                            onDiscardObjectCard();
                        }
                    }
                    model.sectorEffect(carta.getType());
                }
            }

        }
        if (model.getChangePositionEffect()) {
            view.setEndturnButton(true);
        }
    }

    /**
     * Method that open the object cards dialog by clicking the express button
     */
    public void onUseObjectCard() {
        view.setUseObjectDialog();
        if (model.getPlayerById(description.getId()).getObjectsOwned().isEmpty()) {
            view.setObjectButton(false);
        }
    }

    public Position getEscapehatchsectorbroken() {
        return escapehatchsectorbroken;
    }

    public void onSpotlightPosition(Position position) {
        view.setSpotlightPositionDialog(false);
        List<Integer> playersonsector;
        playersonsector = model.getPlayerOnSector(position);
        view.revelPlayersOnSector(playersonsector, position);
        Position adjacentPosition;
        List<Position> adjacentSectors = model.getMap().getAdjacentSector(position);
        for (int j = 0; j < adjacentSectors.size(); j++) {
            adjacentPosition = adjacentSectors.get(j);
            playersonsector = model.getPlayerOnSector(adjacentPosition);
            view.revelPlayersOnSector(playersonsector, adjacentPosition);
        }
    }

    public void onDiscardObjectCard() {
        view.setDiscarObjectDialog();
        if (model.getPlayerById(description.getId()).getObjectsOwned().isEmpty()) {
            view.setObjectButton(false);
        }
    }

    public void discardObjectCard(Integer type) {
        Players player = model.getPlayerById(description.getId());
        String namecard = player.getObjectCard(type).getTypeString(type);
        model.removeObjectCard(type, player);
        model.setObjectDiscarded(type);
        view.messageToPlayer("La carta " + namecard + " è stata scartata!");
    }

    /**
     * Method that calls the alienAttack when the alien player clicks on the attack button
     */
    public void onAttack() {
        model.alienAttack();
        view.setAttackButton();
    }

    public void onFakePosition(Position fakeposition) {
        view.setFakePositionDialog(false);
        view.notifyFakePosition(fakeposition);
    }

    public void onHumanWin() {
        view.messageToPlayer("Hai pescato una carta scialuppa verde, complimenti hai vinto!");
        model.playerWin();
        view.highlightsWinnerTextField(description.getId() - 1);
    }

    /**
     * Method that updates the current player and start a new turn when the player clicks on end turn
     */
    public void onEndTurn() {
        PlayerDescription olddescription;
        view.setEndturnButton(false);
        if (model.getHumanNumber() == 0 && model.getHumanWinner() == 0) {
            view.messageToPlayer("Tutti gli umani sono morti! Gli alieni vincono!");
            view.setHistoryButton(false);
            return;
        } else if (model.getHumanNumber() == 0 && model.getHumanWinner() == model.getHumanPlayer()) {
            view.messageToPlayer("Tutti gli umani sono scappati! Gli alieni perdono!");
            view.setHistoryButton(false);
            return;
        } else if (model.getHumanNumber() == 0 && model.getHumanWinner() != model.getHumanPlayer()) {
            view.messageToPlayer("Gli alieni vincono ma, qualche umano è riuscito a scappare!");
            view.setHistoryButton(false);
            return;
        }
        if (!(model.getTurnnumber() == MAXTURNS)) {
            olddescription = description;
            description = model.getNextPlayer();
            if (model.getPlayerById(description.getId()).getPlayersDeath() || model.getPlayerById(description.getId()).getPlayersWin()) {
                description = model.getNextPlayer();
            }
            view.setCurrentPlayerDescription(description);
            view.messageToPlayer("Tocca al giocatore " + description.getName());
            view.removehighlightsTextField(olddescription);
            startTurn();
        } else {
            view.messageToPlayer("Il gioco è arrivato alla fine! La squadra degli alieni vince!");
            view.setHistoryButton(false);
        }
    }

    /**
     * Turn's starter for the next player
     */
    public void startTurn() {
        if (model.getFirstplayerID() == description.getId()) {
            model.incrementTurnnumber();
        }
        view.setUIButtons();
        for (int i = 0; i < model.getPlayersNumber(); i++) {
            if (model.getPlayerById(i + 1).getPlayersDeath()) {
                view.highlightsDeathTextField(i);
            } else if (model.getPlayerById(i + 1).getPlayersWin()) {
                view.highlightsWinnerTextField(i);
            }
        }
        view.highlightsTextField();
        view.printMessageQuee(description.getId());
    }

    /**
     * Method that opens the dialog for the history positions list
     */
    public void onHistoryPosition() {
        view.setHistoryPositionDialog();
    }

    /**
     * Method that calls an alien attack for the human player that uses the attack object card and remove the card from the objects owned list
     */
    public void onAttackCard() {
        Players player = model.getPlayerById(model.getCurrentPlayer());
        model.alienAttack();
        model.removeObjectCard(ATTACK, player);
    }

    /**
     * Method that opens the spotlight dialog and remove the card from the objects owned list
     */
    public void onSpotlightCard() {
        Players player = model.getPlayerById(model.getCurrentPlayer());
        view.setSpotlightPositionDialog(true);
        model.removeObjectCard(SPOTLIGHT, player);
    }

    /**
     * Method that sets the sedative value to true when the human player uses the sedative object card and remove the card from the objects owned list
     */
    public void onSedativeCard() {
        Players player = model.getPlayerById(model.getCurrentPlayer());
        player.setSedative(true);
        model.removeObjectCard(SEDATIVE, player);
    }

    /**
     * Method that sets the current position of the player to his initial position, setting to true the teleport value and calling board.checkExistingPosition; then remove the card from the objects owned list
     */
    public void onTeleportCard() {
        Players player = model.getPlayerById(model.getCurrentPlayer());
        player.setTeleport(true);
        model.getMap().checkExistingPosition(player, new Position('l', 8));
        model.removeObjectCard(TELEPORT, player);
        player.setTeleport(false);
    }

    /**
     * Method that verifies that the card is used before the movement and then sets the adrenaline value to true; then remove the card from the objects owned list
     */
    public void onAdrenalineCard() {
        Players player = model.getPlayerById(model.getCurrentPlayer());
        if (model.getChangePositionEffect()) {
            view.messageToPlayer("Questa carta si può utilizzare solo prima di muoversi!");
            return;
        }
        player.setAdrenaline(true);
        model.removeObjectCard(ADRENALINE, player);
    }
}


