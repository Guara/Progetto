package it.polimi.ingsw.progetto.model.decks;

import it.polimi.ingsw.progetto.model.cards.EscapeHatchCards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for escape hatch deck
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class EscapeHatchDeck extends Decks {
   private static final int DIMENSION = 6;
   private List<EscapeHatchCards> cardsarray = new ArrayList<EscapeHatchCards> (DIMENSION);
   private String color;
   /**
    * Constructor
    */
   public EscapeHatchDeck() {
      for(max=0; max<DIMENSION; max++){
         //three cards red then three cards green
         if (max < 3){
            color = "green";
         } else {
            color = "red";
         }
         cardsarray.add(new EscapeHatchCards(color));
      }
      //mix the deck
      Collections.shuffle(cardsarray);
   }
   
   /**
    * Method that creates a new escape hatch deck with six escape hatch cards
    * @return a list of escapehatchcards
    */


   /**
    * Method that take and remove a card from the deck
    * @return a card from the deck
    */
   public EscapeHatchCards getCard() {
      EscapeHatchCards carta= cardsarray.get(0);
      cardsarray.remove(0);
      return carta;
   }
   
   public void addEscapeHatchCard(EscapeHatchCards carta) {
      cardsarray.add(carta);
   }
   
}
