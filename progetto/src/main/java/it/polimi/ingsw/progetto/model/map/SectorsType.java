package it.polimi.ingsw.progetto.model.map;

/**
 * enumerates all sector types in the map
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public enum SectorsType {
SECURE, DANGEROUS, HUMAN, ALIEN, ESCAPEHATCH, NONE
}
