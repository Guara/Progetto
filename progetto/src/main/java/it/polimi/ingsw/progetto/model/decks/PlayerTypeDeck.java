package it.polimi.ingsw.progetto.model.decks;

import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for player type deck
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class PlayerTypeDeck extends Decks {

   private List<PlayerTypeCards> cardsarray;

   /**
    * Constructor
    * @param playersnumber
    */
   public PlayerTypeDeck(int playersnumber) {
      cardsarray = new ArrayList<PlayerTypeCards>();
      cardsarray = setPlayerTypeDeck(playersnumber);
   }

   /**
    * Method that creates a new player type deck with playersnumber player type cards
    * @param playersnumber, number of players
    * @return
    */
   public List<PlayerTypeCards> setPlayerTypeDeck(int playersnumber) {
      for (int i = 0; i < playersnumber; i++) {
         cardsarray.add(PlayerTypeCards.addPlayerTypeCard());
      }
      //mix the deck
      Collections.shuffle(cardsarray);
      return cardsarray;
   }

   public List<PlayerTypeCards> getArray() {
      return cardsarray;
   }

   /**
    * Method that take and remove a card from the deck
    * @param deck
    * @return a card from the deck
    */
   public PlayerTypeCards drawPlayerTypeCard(List<PlayerTypeCards> deck) {
      PlayerTypeCards card = deck.get(0);
      deck.remove(card);
      return card;
   }
}
