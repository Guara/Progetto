package it.polimi.ingsw.progetto.model.map;

import it.polimi.ingsw.progetto.model.players.*;

import java.util.*;

/**
 * Class that implements the map, an hashmap with key type position and value type an ArrayList of id (integer)
 *
 * @author Luigi Davide Greco, Daniele Guarascio
 */
public class Board extends Observable {
    private boolean changed = false;
    private static Map<Position, ArrayList<Integer>> players;
    private static List<Position> positionsadded;

    /**
     * Map's constructor
     */
    public Board() {
        this.players = new HashMap<Position, ArrayList<Integer>>();
        this.positionsadded = new ArrayList<Position>();
    }

    /**
     * Method that add a new player in the map
     *
     * @param player object that is added in the map
     */
    public static void setNewPlayer(Players player) {
        int id = player.getID();
        List<Integer> giocatori = new ArrayList<Integer>();
        giocatori.add(id);
        players.put(player.getPosition(), (ArrayList<Integer>) giocatori);
        positionsadded.add(player.getPosition());
    }

    /**
     * @param position in which we want to know if there's some player
     * @return a list of ID
     */

    public List<Integer> getID(Position position) {
        int searchcolumn = (int) position.getColumn();
        int searchline = position.getLine();
        for (int posizione = 0; posizione < positionsadded.size(); posizione++) {
            int column = (int) positionsadded.get(posizione).getColumn();
            int line = positionsadded.get(posizione).getLine();
            if (column == searchcolumn && line == searchline) {
                return players.get(positionsadded.get(posizione));
            }
        }
        return null;
    }

    /**
     * Method that changes the position of the current player
     *
     * @param player      that is going to change his position
     * @param newposition where player want to go in the map
     */
    public boolean setPosition(Players player, Position newposition) {
        changed = false;
        setSector(newposition);
        int newcolumn = (int) newposition.getColumn();
        int newline = newposition.getLine();
        //control if newposition is already inside the map: in that case only update the key's value
        if (newposition.getEscapeHatchBroken())
            return false;

        Position thisposition = player.getPosition();
        int thiscolumn = (int) thisposition.getColumn();
        int thisline = thisposition.getLine();

        if (newcolumn == thiscolumn && newline == thisline) {
            return false;
        }
        if (!checkLimits(newcolumn, newline)) {
            return false;
        }
        if (player instanceof AlienPlayer || player.getAdrenaline()) {
            player.setAdrenaline(false);
            if (newline == thisline && (newcolumn == thiscolumn - 1 || newcolumn == thiscolumn + 1)) {
                if (!checkExistingPosition(player, newposition)) {
                    addPositionToMap(player, newposition);
                    positionsadded.add(newposition);
                }
            } else if ((newline == thisline + 1 || newline == thisline - 1) && (newcolumn == thiscolumn || newcolumn == thiscolumn - 1 || newcolumn == thiscolumn + 1)) {
                if (!checkExistingPosition(player, newposition)) {
                    addPositionToMap(player, newposition);
                    positionsadded.add(newposition);
                }
            }

            if (checkCrossing(thisposition, newposition)) {
                if (newline == thisline - 2 && (newcolumn == thiscolumn || newcolumn == thiscolumn - 1 || newcolumn == thiscolumn + 1)) {
                    if (!checkExistingPosition(player, newposition)) {
                        addPositionToMap(player, newposition);
                        positionsadded.add(newposition);
                    }
                } else if (newline == thisline + 2 && (newcolumn == thiscolumn || newcolumn == thiscolumn + 1 || newcolumn == thiscolumn - 1)) {
                    if (!checkExistingPosition(player, newposition)) {
                        addPositionToMap(player, newposition);
                        positionsadded.add(newposition);
                    }
                } else if ((newline == thisline + 1 || newline == thisline - 1) && (newcolumn == thiscolumn - 2 || newcolumn == thiscolumn + 2)) {
                    if (!checkExistingPosition(player, newposition)) {
                        addPositionToMap(player, newposition);
                        positionsadded.add(newposition);
                    }
                } else if (newline == thisline && (newcolumn == thiscolumn - 2 || newcolumn == thiscolumn + 2)) {
                    if (!checkExistingPosition(player, newposition)) {
                        addPositionToMap(player, newposition);
                        positionsadded.add(newposition);
                    }
                }
            }
            if (player.getAlienFeeding()) {
                if ((!checkCrossing(thisposition, newposition)) && (checkCrossingToGetAroundObstacles(thisposition, newposition))) {
                    if (newcolumn == thiscolumn && (newline == thisline + 2 || newline == thisline - 2)) {
                        if (!checkExistingPosition(player, newposition)) {
                            addPositionToMap(player, newposition);
                            positionsadded.add(newposition);
                        }
                    } else if ((newcolumn == thiscolumn + 2 || newcolumn == thiscolumn - 2) && (newline == thisline - 1 || newline == thisline + 1)) {
                        if (!checkExistingPosition(player, newposition)) {
                            addPositionToMap(player, newposition);
                            positionsadded.add(newposition);
                        }
                    }
                }
                if (checkCrossingFor3Steps(thisposition, newposition)) {
                    if (newcolumn == thiscolumn && (newline == thisline - 3) || (newline == thisline + 3)) {
                        if (!checkExistingPosition(player, newposition)) {
                            addPositionToMap(player, newposition);
                            positionsadded.add(newposition);
                        }
                    } else if ((newcolumn == thiscolumn - 3 || newcolumn == thiscolumn + 3) && (newline == thisline + 1 || newline == thisline + 2 || newline == thisline - 1 || newline == thisline || newline == thisline - 2)) {
                        if (!checkExistingPosition(player, newposition)) {
                            addPositionToMap(player, newposition);
                            positionsadded.add(newposition);
                        }
                    } else if ((newcolumn == thiscolumn + 1 || newcolumn == thiscolumn - 1) && (newline == thisline + 2 || newline == thisline - 3 || newline == thisline - 2) || newline == thisline + 3) {
                        if (!checkExistingPosition(player, newposition)) {
                            addPositionToMap(player, newposition);
                            positionsadded.add(newposition);
                        }
                    } else if ((newcolumn == thiscolumn + 2 || newcolumn == thiscolumn - 2) && (newline == thisline + 2 || newline == thisline - 2)) {
                        if (!checkExistingPosition(player, newposition)) {
                            addPositionToMap(player, newposition);
                            positionsadded.add(newposition);
                        }
                    }
                }
            }
        } else if (player instanceof HumanPlayer) {
            if (newline == thisline && (newcolumn == thiscolumn - 1 || newcolumn == thiscolumn + 1)) {
                if (!checkExistingPosition(player, newposition)) {
                    addPositionToMap(player, newposition);
                    positionsadded.add(newposition);
                }
            } else if ((newline == thisline + 1 || newline == thisline - 1) && (newcolumn == thiscolumn || newcolumn == thiscolumn - 1 || newcolumn == thiscolumn + 1)) {
                if (!checkExistingPosition(player, newposition)) {
                    addPositionToMap(player, newposition);
                    positionsadded.add(newposition);
                }
            }
        }
        return changed;
    }

    /**
     * Method that controls if a position is settable
     *
     * @param newcolumn of the position that the method controls
     * @param newline   of the position that the method controls
     * @return true if is settable
     */
    public boolean checkLimits(int newcolumn, int newline) {
        //higher and lower margins control
        switch (newcolumn) {
            case 101:
            case 115:
                if (newline < 2 || newline > 13) {
                    //print error
                    return false;
                }
                break;
            case 102:
                if (newline < 1 || newline > 11) {
                    //print error
                    return false;
                }
                break;
            case 114:
                if (newline < 1 || newline > 13) {
                    //print error
                    return false;
                }
                break;
            case 97:
            case 100:
            case 111:
            case 116:
            case 119:
                if (newline < 2 || newline > 14) {
                    //print error
                    return false;
                }
                break;
            default:
                if (newline < 1 || newline > 14) {
                    //print error
                    return false;
                }
                break;
        }
        //side margins control
        switch (newline) {
            case 1:
                if (newcolumn < 98 || newcolumn > 118) {
                    //print error
                    return false;
                }
                break;
            case 7:
                if (newcolumn < 99 || newcolumn > 117) {
                    //print error
                    return false;
                }
                break;
            case 8:
                if (newcolumn < 98 || newcolumn > 117) {
                    //print error
                    return false;
                }
                break;
            default:
                if (newcolumn < 97 || newcolumn > 119) {
                    //print error
                    return false;
                }
                break;
        }
        //bad sectors control
        if (newline == 3 && (newcolumn == 100 || newcolumn == 111 || newcolumn == 115 || newcolumn == 116)) {
            //print error
            return false;
        }
        if (newline == 4 && (newcolumn == 100 || newcolumn == 111 || newcolumn == 116)) {
            //print error
            return false;
        }
        if (newline == 5 && newcolumn == 104) {
            //print error
            return false;
        }
        if (newline == 6 && (newcolumn == 100 || newcolumn == 105 || newcolumn == 108)) {
            //print error
            return false;
        }
        if (newline == 7 && (newcolumn == 100 || newcolumn == 101 || newcolumn == 106 || newcolumn == 107 || newcolumn == 108 || newcolumn == 109 || newcolumn == 110)) {
            //print error
            return false;
        }
        if (newline == 8 && (newcolumn == 97 || newcolumn == 108 || newcolumn == 119)) {
            //print error
            return false;
        }
        if (newline == 9 && newcolumn == 116) {
            //print error
            return false;
        }
        if (newline == 10 && ((newcolumn == 104) || (newcolumn == 114) || (newcolumn == 115) || (newcolumn == 116))) {
            //print error
            return false;
        }
        if (newline == 11 && (newcolumn == 104 || newcolumn == 114 || newcolumn == 115)) {
            //print error
            return false;
        }
        if (newline == 12 && (newcolumn == 105 || newcolumn == 106)) {
            //print error
            return false;
        }
        if (newline == 13 && newcolumn == 107) {
            //print error
            return false;
        }
        return true;
    }

    /**
     * Method to set the sector's type when a new position is created
     *
     * @param posizione
     */
    public void setSector(Position posizione) {
        int thiscolumn = (int) posizione.getColumn();
        int thisline = posizione.getLine();
        switch (thiscolumn) {
            case 97:
                if (thisline == 2 || thisline == 3 || thisline == 14) {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                } else {
                    posizione.setSectorType(SectorsType.SECURE);
                }
                break;
            case 98:
                if (thisline == 5 || thisline == 10) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else if (thisline == 2 || thisline == 13) {
                    posizione.setSectorType(SectorsType.ESCAPEHATCH);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 106:
            case 99:
                if (thisline == 1 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 100:
                if (thisline == 10 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 101:
                if (thisline == 2 || thisline == 12) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 102:
                if (thisline == 1 || thisline == 10) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 103:
                if (thisline == 7 || thisline == 12 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 104:
                if (thisline == 1 || thisline == 2 || thisline == 3 || thisline == 7 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 105:
                if (thisline == 1 || thisline == 9 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 109:
            case 107:
                if (thisline == 2 || thisline == 5 || thisline == 9 || thisline == 11) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 108:
                if (thisline == 2 || thisline == 4 || thisline == 9 || thisline == 11 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 110:
                if (thisline == 3 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 111:
                if (thisline == 5 || thisline == 9 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 112:
                if (thisline == 1 || thisline == 3 || thisline == 4 || thisline == 12) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 113:
                if (thisline == 1 || thisline == 4 || thisline == 5 || thisline == 11 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 114:
                if (thisline == 2 || thisline == 3 || thisline == 5 || thisline == 9 || thisline == 13) {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                } else {
                    posizione.setSectorType(SectorsType.SECURE);
                }
                break;
            case 115:
                posizione.setSectorType(SectorsType.DANGEROUS);
                break;
            case 116:
                if (thisline == 7 || thisline == 8 || thisline == 14) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 117:
                if (thisline == 1 || thisline == 5 || thisline == 12) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 118:
                if (thisline == 1 || thisline == 8) {
                    posizione.setSectorType(SectorsType.SECURE);
                } else if (thisline == 2 || thisline == 13) {
                    posizione.setSectorType(SectorsType.ESCAPEHATCH);
                } else {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                }
                break;
            case 119:
                if (thisline == 2 || thisline == 9 || thisline == 13 || thisline == 14) {
                    posizione.setSectorType(SectorsType.DANGEROUS);
                } else {
                    posizione.setSectorType(SectorsType.SECURE);
                }
                break;
        }
    }

    /**
     * Method to find adjacent sectors of posizione, used for spotlight card
     *
     * @param posizione where looking for adjacent sectors
     * @return list of positions
     */
    public List<Position> getAdjacentSector(Position posizione) {
        int thiscolumn = (int) posizione.getColumn();
        int thisline = posizione.getLine();
        List<Position> adjacentSectors = new ArrayList<Position>();
        int newcolumn;
        int newline;
        char finalcolumn;

        newcolumn = thiscolumn;
        newline = thisline + 1;
        if (checkLimits(newcolumn, newline)) {
            finalcolumn = (char) newcolumn;
            posizione = new Position(finalcolumn, newline);
            adjacentSectors.add(posizione);
        }

        newcolumn = thiscolumn;
        newline = thisline - 1;
        if (checkLimits(newcolumn, newline)) {
            finalcolumn = (char) newcolumn;
            posizione = new Position(finalcolumn, newline);
            adjacentSectors.add(posizione);
        }

        newcolumn = thiscolumn + 1;
        newline = thisline;
        if (checkLimits(newcolumn, newline)) {
            finalcolumn = (char) newcolumn;
            posizione = new Position(finalcolumn, newline);
            adjacentSectors.add(posizione);
        }

        newcolumn = thiscolumn - 1;
        newline = thisline;
        if (checkLimits(newcolumn, newline)) {
            finalcolumn = (char) newcolumn;
            posizione = new Position(finalcolumn, newline);
            adjacentSectors.add(posizione);
        }

        if (thiscolumn % 2 == 0) {
            newcolumn = thiscolumn + 1;
            newline = thisline + 1;
            if (checkLimits(newcolumn, newline)) {
                finalcolumn = (char) newcolumn;
                posizione = new Position(finalcolumn, newline);
                adjacentSectors.add(posizione);
            }

            newcolumn = thiscolumn - 1;
            newline = thisline + 1;
            if (checkLimits(newcolumn, newline)) {
                finalcolumn = (char) newcolumn;
                posizione = new Position(finalcolumn, newline);
                adjacentSectors.add(posizione);
            }
        } else if (thiscolumn % 2 != 0) {
            newcolumn = thiscolumn + 1;
            newline = thisline - 1;
            if (checkLimits(newcolumn, newline)) {
                finalcolumn = (char) newcolumn;
                posizione = new Position(finalcolumn, newline);
                adjacentSectors.add(posizione);
            }

            newcolumn = thiscolumn - 1;
            newline = thisline - 1;
            if (checkLimits(newcolumn, newline)) {
                finalcolumn = (char) newcolumn;
                posizione = new Position(finalcolumn, newline);
                adjacentSectors.add(posizione);
            }
        }
        return adjacentSectors;
    }

    /**
     * Method that remove a player from a position, remove his id from the list of integers
     *
     * @param thisposition where remove the current player
     * @param id           of the current player
     */
    public void removeIDFromThisPosition(Position thisposition, int id) {
        List<Integer> giocatorisusettore = players.get(thisposition);
        for (int i = 0; i < giocatorisusettore.size(); i++) {
            if (giocatorisusettore.get(i) == id) {
                giocatorisusettore.remove(i);
            }
        }
        players.put(thisposition, (ArrayList<Integer>) giocatorisusettore);
    }

    /**
     * Method that controls the empty sectors' crossing in the two sectors' movement
     *
     * @param thisposition where is the player before the movement
     * @param newposition  where he wants to go with the movement
     * @return true if it's a valid movement
     */
    boolean checkCrossing(Position thisposition, Position newposition) {
        int newline = newposition.getLine();
        int newcolumn = (int) newposition.getColumn();
        int thisline = thisposition.getLine();
        int thiscolumn = (int) thisposition.getColumn();
        //controls are carried out on the movement, sector by sector
        if (newcolumn == thiscolumn) {
            if (newline == thisline - 2) {
                if (!checkLimits(thiscolumn, thisline - 1)) {
                    return false;
                }
            }
            if (newline == thisline + 2) {
                if (!checkLimits(thiscolumn, thisline + 1)) {
                    return false;
                }
            }
        }
        if (newcolumn % 2 == 0) {
            if (newcolumn == thiscolumn + 1) {
                if (newline == thisline - 1) {
                    if (!checkLimits(thiscolumn, thisline - 1) && !checkLimits(thiscolumn + 1, thisline)) {
                        return false;
                    }
                }
                if (newline == thisline + 2) {
                    if (!checkLimits(thiscolumn, thisline + 1) && !checkLimits(thiscolumn + 1, thisline + 1)) {
                        return false;
                    }
                }
            }
            if (newcolumn == thiscolumn - 1) {
                if (newline == thisline - 1) {
                    if (!checkLimits(thiscolumn, thisline - 1) && !checkLimits(thiscolumn - 1, thisline)) {
                        return false;
                    }
                }
                if (newline == thisline + 2) {
                    if (!checkLimits(thiscolumn, thisline + 1) && !checkLimits(thiscolumn - 1, thisline + 1)) {
                        return false;
                    }
                }
            }
            if (newcolumn == thiscolumn + 2) {
                if (newline == thisline - 1) {
                    if (!checkLimits(thiscolumn + 1, thisline)) {
                        return false;
                    }
                }
                if (newline == thisline) {
                    if (!checkLimits(thiscolumn + 1, thisline) && !checkLimits(thiscolumn + 1, thisline + 1)) {
                        return false;
                    }
                }
                if (newline == thisline + 1) {
                    if (!checkLimits(thiscolumn + 1, thisline + 1)) {
                        return false;
                    }
                }
            }
            if (newcolumn == thiscolumn - 2) {
                if (newline == thisline - 1) {
                    if (!checkLimits(thiscolumn - 1, thisline)) {
                        return false;
                    }
                }
                if (newline == thisline) {
                    if (!checkLimits(thiscolumn - 1, thisline) && !checkLimits(thiscolumn - 1, thisline + 1)) {
                        return false;
                    }
                }
                if (newline == thisline + 1) {
                    if (!checkLimits(thiscolumn - 1, thisline + 1)) {
                        return false;
                    }
                }
            }
        }
        if (newcolumn % 2 != 0) {
            if (newcolumn == thiscolumn + 1) {
                if (newline == thisline - 2) {
                    if (!checkLimits(thiscolumn, thisline - 1) && !checkLimits(thiscolumn + 1, thisline - 1)) {
                        return false;
                    }
                }
                if (newline == thisline + 1) {
                    if (!checkLimits(thiscolumn, thisline + 1) && !checkLimits(thiscolumn + 1, thisline)) {
                        return false;
                    }
                }
            }
            if (newcolumn == thiscolumn - 1) {
                if (newline == thisline - 2) {
                    if (!checkLimits(thiscolumn, thisline - 1) && !checkLimits(thiscolumn - 1, thisline - 1)) {
                        return false;
                    }
                }
                if (newline == thisline + 1) {
                    if (!checkLimits(thiscolumn, thisline + 1) && !checkLimits(thiscolumn - 1, thisline)) {
                        return false;
                    }
                }
            }
            if (newcolumn == thiscolumn + 2) {
                if (newline == thisline - 1) {
                    if (!checkLimits(thiscolumn + 1, thisline - 1)) {
                        return false;
                    }
                }
                if (newline == thisline) {
                    if (!checkLimits(thiscolumn + 1, thisline - 1) && !checkLimits(thiscolumn + 1, thisline)) {
                        return false;
                    }
                }
                if (newline == thisline + 1) {
                    if (!checkLimits(thiscolumn + 1, thisline)) {
                        return false;
                    }
                }
            }
            if (newcolumn == thiscolumn - 2) {
                if (newline == thisline - 1) {
                    if (!checkLimits(thiscolumn - 1, thisline - 1)) {
                        return false;
                    }
                }
                if (newline == thisline) {
                    if (!checkLimits(thiscolumn - 1, thisline - 1) && !checkLimits(thiscolumn - 1, thisline)) {
                        return false;
                    }
                }
                if (newline == thisline + 1) {
                    if (!checkLimits(thiscolumn - 1, thisline)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Method that controls the empty sectors' crossing in the three sectors' movement
     *
     * @param thisposition where is the player before the movement
     * @param newposition  where he wants to go with the movement
     * @return true if it's a valid movement
     */
    public boolean checkCrossingFor3Steps(Position thisposition, Position newposition) {
        int thiscolumn = (int) thisposition.getColumn();
        int thisline = thisposition.getLine();
        int newcolumn = (int) newposition.getColumn();
        int newline = newposition.getLine();
        //controls are carried out on the movement, sector by sector
        if (newcolumn == thiscolumn) {
            if (newline == thisline + 3) {
                if (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn, thisline + 2))
                    return false;
            }
            if (newline == thisline - 3) {
                if (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn, thisline - 2))
                    return false;
            }
        } else if (newcolumn == thiscolumn - 3) {
            if (newline == thisline + 2) {
                if (!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 2, thisline + 1))
                    return false;
            }
            if (newline == thisline + 1) {
                if (thiscolumn % 2 == 0) {
                    if ((!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline)) && (!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 2, thisline + 1)) && (!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 2, thisline)))
                        return false;
                } else if (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline + 1))
                    return false;
            }
            if (newline == thisline - 1) {
                if (thiscolumn % 2 == 0) {
                    if (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline - 1))
                        return false;
                } else if ((!checkLimits(thiscolumn - 1, thisline - 1) || !checkLimits(thiscolumn - 2, thisline - 1)) && (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline)) && (!checkLimits(thiscolumn - 1, thisline - 1) || !checkLimits(thiscolumn - 2, thisline)))
                    return false;
            }
            if (newline == thisline) {
                if (thiscolumn % 2 == 0) {
                    if (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline - 1) && (!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 2, thisline)) && (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline)))
                        return false;
                } else if (!checkLimits(thiscolumn - 1, thisline - 1) || !checkLimits(thiscolumn - 2, thisline) && (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline + 1)) && (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline)))
                    return false;
            }
            if (newline == thisline - 2) {
                if (!checkLimits(thiscolumn - 1, thisline - 1) || !checkLimits(thiscolumn - 2, thisline - 1))
                    return false;
            }
        } else if (newcolumn == thiscolumn + 3) {
            if (newline == thisline + 2) {
                if (!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 2, thisline + 1))
                    return false;
            }
            if (newline == thisline - 1) {
                if (thiscolumn % 2 != 0) {
                    if (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline) && (!checkLimits(thiscolumn + 1, thisline - 1) || !checkLimits(thiscolumn + 2, thisline - 1)) && (!checkLimits(thiscolumn + 1, thisline - 1) || !checkLimits(thiscolumn + 2, thisline)))
                        return false;
                } else if (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline - 1))
                    return false;
            }
            if (newline == thisline + 1) {
                if (thiscolumn % 2 == 0) {
                    if ((!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline)) && (!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 2, thisline + 1)) && (!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 2, thisline)))
                        return false;
                } else if (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline + 1))
                    return false;
            }
            if (newline == thisline) {
                if (thiscolumn % 2 == 0) {
                    if ((!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline - 1)) && (!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 2, thisline)) && (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline)))
                        return false;
                } else if ((!checkLimits(thiscolumn + 1, thisline - 1) || !checkLimits(thiscolumn + 2, thisline)) && (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline + 1)) && (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline)))
                    return false;
            }
            if (newline == thisline - 2) {
                if (!checkLimits(thiscolumn + 1, thisline - 1) || !checkLimits(thiscolumn + 2, thisline - 1))
                    return false;
            }
        } else if (newcolumn == thiscolumn + 1) {
            if (newline == thisline + 2) {
                if ((!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 1, thisline + 1)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn, thisline + 2)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn + 1, thisline + 1)))
                    return false;
            }
            if (newline == thisline - 3) {
                if ((!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn, thisline - 2)) && (!checkLimits(thiscolumn + 1, thisline - 1) || !checkLimits(thiscolumn + 1, thisline - 2)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn + 1, thisline - 2)))
                    return false;
            }
            if (newline == thisline + 3) {
                if ((!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 1, thisline + 2)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn, thisline + 2)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn + 1, thisline + 2)))
                    return false;
            }
            if (newline == thisline - 2) {
                if ((!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 1, thisline - 1)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn, thisline - 2)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn + 1, thisline - 1)))
                    return false;
            }
        } else if (newcolumn == thiscolumn - 1) {
            if (newline == thisline + 2) {
                if ((!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn, thisline + 2)) && (!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 1, thisline + 1)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn - 1, thisline + 1)))
                    return false;
            }
            if (newline == thisline - 3) {
                if ((!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn, thisline - 2)) && (!checkLimits(thiscolumn - 1, thisline - 1) || !checkLimits(thiscolumn - 1, thisline - 2)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn - 1, thisline - 2)))
                    return false;
            }
            if (newline == thisline + 3) {
                if ((!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 1, thisline + 2)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn, thisline + 2)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn - 1, thisline + 2)))
                    return false;
            }
            if (newline == thisline - 2) {
                if ((!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 1, thisline - 1)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn, thisline - 2)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn - 1, thisline - 1)))
                    return false;
            }
        } else if (newcolumn == thiscolumn + 2) {
            if (newline == thisline + 2) {
                if ((!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 2, thisline + 1)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn + 1, thisline + 2)) && (!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 1, thisline + 2)))
                    return false;
            }
            if (newline == thisline - 2) {
                if ((!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline - 1)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn + 1, thisline - 1)) && (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 1, thisline - 1)))
                    return false;
            }
        } else if (newcolumn == thiscolumn - 2) {
            if (newline == thisline + 2) {
                if ((!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 2, thisline + 1)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn - 1, thisline + 2)) && (!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 1, thisline + 2)))
                    return false;
            }
            if (newline == thisline - 2) {
                if ((!checkLimits(thiscolumn - 1, thisline - 1) || !checkLimits(thiscolumn - 2, thisline - 1)) && (!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn - 1, thisline - 2)) && (!checkLimits(thiscolumn - 1, thisline - 1) || !checkLimits(thiscolumn - 1, thisline - 2)))
                    return false;
            }
        }
        return true;
    }

    /**
     * Method that controls the empty sectors' crossing in the three sectors' movements to avoid obstacles
     *
     * @param thisposition where is the player before the movement
     * @param newposition  where is the player before the movement
     * @return true if it's a valid movement
     */
    public boolean checkCrossingToGetAroundObstacles(Position thisposition, Position newposition) {
        int thiscolumn = (int) thisposition.getColumn();
        int thisline = thisposition.getLine();
        int newcolumn = (int) newposition.getColumn();
        int newline = newposition.getLine();
        //controls are carried out on the movement, sector by sector
        if (newcolumn == thiscolumn) {
            if (newline == thisline - 2) {
                if ((!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 1, thisline - 1)) && (!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 1, thisline - 1)))
                    return false;
            }
            if (newline == thisline + 2) {
                if ((!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 1, thisline + 2)) && (!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 1, thisline + 2)))
                    return false;
            }
        } else if (newcolumn == thiscolumn + 2) {
            if (newline == thisline - 1) {
                if ((!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn + 1, thisline - 1)) && (!checkLimits(thiscolumn + 1, thisline + 1) || !checkLimits(thiscolumn + 2, thisline)))
                    return false;
            }
            if (newline == thisline + 1) {
                if ((!checkLimits(thiscolumn + 1, thisline) || !checkLimits(thiscolumn + 2, thisline)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn + 1, thisline + 2)))
                    return false;
            }
        } else if (newcolumn == thiscolumn - 2) {
            if (newline == thisline - 1) {
                if ((!checkLimits(thiscolumn, thisline - 1) || !checkLimits(thiscolumn - 1, thisline - 1)) && (!checkLimits(thiscolumn - 1, thisline + 1) || !checkLimits(thiscolumn - 2, thisline)))
                    return false;
            }
            if (newline == thisline + 1) {
                if ((!checkLimits(thiscolumn - 1, thisline) || !checkLimits(thiscolumn - 2, thisline)) && (!checkLimits(thiscolumn, thisline + 1) || !checkLimits(thiscolumn - 1, thisline + 2)))
                    return false;
            }
        }
        return true;
    }

    /**
     * Method that puts a newposition in the board and remove from previous position the player's id
     *
     * @param player
     * @param newposition
     */
    public void addPositionToMap(Players player, Position newposition) {
        int id = player.getID();
        Position thisposition = player.getPosition();
        List<Integer> giocatore = players.get(newposition);
        if (giocatore == null) {
            giocatore = new ArrayList<Integer>();
        }
        giocatore.add(id);
        players.put(newposition, (ArrayList<Integer>) giocatore);
        removeIDFromThisPosition(thisposition, id);
        player.setPosition(newposition);
        changed = true;
    }

    /**
     * Method useful for test
     *
     * @param posizione
     * @param lista
     */
    public void put(Position posizione, List<Integer> lista) {
        players.put(posizione, (ArrayList<Integer>) lista);
        positionsadded.add(posizione);
    }

    /**
     * Method useful for test
     *
     * @param posizione
     * @return
     */
    public List<Integer> getPlayersInThisPosition(Position posizione) {
        return players.get(posizione);
    }

    /**
     * Method that checks if the newposition is already inside the map: in that case take that object and simply updates the value of that key calling addPositionToMap
     *
     * @param player      that is going to move
     * @param newposition where the player is going to move
     * @return true if newposition is already inside the map
     */

    public boolean checkExistingPosition(Players player, Position newposition) {
        int newcolumn = (int) newposition.getColumn();
        int newline = newposition.getLine();
        for (int posizione = 0; posizione < positionsadded.size(); posizione++) {
            int column = (int) positionsadded.get(posizione).getColumn();
            int line = positionsadded.get(posizione).getLine();
            if (column == newcolumn && line == newline) {
                if (positionsadded.get(posizione).getEscapeHatchBroken())
                    return false;
                if (column == 'l' && line == 6)
                    return false;
                if (column == 'l' && line == 8 && !player.getTeleport())
                    return false;
                addPositionToMap(player, positionsadded.get(posizione));
                return true;
            }
        }
        return false;
    }
}