package it.polimi.ingsw.progetto.model;

import java.util.*;

import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;
import it.polimi.ingsw.progetto.model.map.Board;
import it.polimi.ingsw.progetto.model.map.Position;
import it.polimi.ingsw.progetto.model.players.AlienPlayer;
import it.polimi.ingsw.progetto.model.players.HumanPlayer;
import it.polimi.ingsw.progetto.model.players.PlayerDescription;
import it.polimi.ingsw.progetto.model.players.Players;
import it.polimi.ingsw.progetto.model.cards.*;
import it.polimi.ingsw.progetto.model.decks.*;
import it.polimi.ingsw.progetto.view.View;

/**
 * Class that contains all data of the game
 *
 * @author Luigi Davide Greco, Daniele Guarascio
 */
public class StateGame extends Observable {
    private Board map;
    private List<Players> players;
    private int playersnumber;
    private int currentplayer;
    private EscapeHatchDeck escapehatch;
    private ObjectDeck object;
    private SectorDeck sector;
    private PlayerTypeDeck typedeck;
    //mazzi per salvare le carte usate
    private ObjectDeck objectdiscarded; //deck useful to save used or discarded object cards
    private List<PlayerDescription> playersDeath;
    private List<PlayerDescription> playerDefense;
    private Players attacker;
    private boolean changePositionEffect;
    private Position attackposition;
    private static int turnnumber = 0;
    private int firstplayerID;

    /**
     * Constructor
     */
    public StateGame() {
        map = new Board();
        players = new ArrayList<Players>();
        playersnumber = 0;
        setCurrentPlayer(0);
        escapehatch = new EscapeHatchDeck();
        object = new ObjectDeck();
        object.createObjectDeck();
        sector = new SectorDeck();
        sector.setSectorDeck();
        objectdiscarded = new ObjectDeck();
        changePositionEffect = false;
    }

    /**
     * Method that add the View class to the Observer list
     *
     * @param view
     */
    public void setObserver(View view) {
        addObserver(view);
        map.addObserver(view);
    }

    /**
     * Method that adds an object player to players' arraylist useful for test
     *
     * @param giocatore
     */
    public void setPlayers(Players giocatore) {
        players.add(giocatore);
    }

    /**
     * Method useful for test
     *
     * @return the first player on the list
     */
    public Players getPlayer() {
        return this.players.get(0);
    }

    public Players getPlayerById(int i) {
        return this.players.get(i - 1);
    }

    public void setPlayerTypeDeck() {
        typedeck = new PlayerTypeDeck(getPlayersNumber());
    }

    public void setNewSectorDeck() {
        sector = new SectorDeck();
        sector.setSectorDeck();
    }

    public PlayerTypeCards drawTypeCard(PlayerTypeDeck deck) {
        PlayerTypeCards card = deck.drawPlayerTypeCard(deck.getArray());
        setChanged();
        notifyObservers(card);
        return card;
    }

    public void setCurrentPlayer(int id) {
        this.currentplayer = id;
    }

    public int getCurrentPlayer() {
        return this.currentplayer;
    }

    public PlayerDescription getPlayerDescripionByID(int id) {
        return new PlayerDescription(getPlayerById(id));
    }

    public void setPlayersNumber(int i) {
        this.playersnumber = i;
    }

    public int getPlayersNumber() {
        return this.playersnumber;
    }

    public EscapeHatchCards drawEscapeHatchCard() {
        return escapehatch.getCard();
    }

    /**
     * Method that sets the object deck with a preset deck, useful for test
     *
     * @param mazzo
     */
    public void setObjectDeck(ObjectDeck mazzo) {
        this.object = mazzo;
    }

    public ObjectCards drawObjectCard() {
        return object.getCard();
    }

    public void setObjectDiscarded(int tipocarta) {
        this.objectdiscarded.add(ObjectCards.getObjectCard(tipocarta));
    }

    /**
     * Method useful for test
     *
     * @return the object cards discarded deck
     */
    public ObjectDeck getObjectDiscarded() {
        return this.objectdiscarded;
    }

    public SectorCards drawSectorCard() {
        SectorCards card = sector.getCard();
        if (card != null) {
            return card;
        }
        setNewSectorDeck();
        card = sector.getCard();
        return card;
    }

    /**
     * Method useful for test
     *
     * @param mazzo
     */
    public void setSectorDeck(SectorDeck mazzo) {
        this.sector = mazzo;
    }

    public void resetObjectDeck() {
        object = objectdiscarded;
        objectdiscarded = new ObjectDeck();
        object.shuffleArray();
    }

    public List<Integer> getPlayerOnSector(Position posizione) {
        return map.getID(posizione);
    }

    public Board getMap() {
        return this.map;
    }

    public PlayerTypeDeck getTypeDeck() {
        return typedeck;
    }

    public void setPlayer(Players player) {
        players.add(player.getID() - 1, player);
        player.setPlayerOnMap();
    }

    public List<String> getName() {
        List<String> names = new ArrayList<String>();
        for (int i = 0; i < players.size(); i++) {
            names.add(players.get(i).getName());
        }
        return names;
    }

    public PlayerDescription getFirstPlayer() {
        final int size = players.size();
        Players player = players.get((int) (Math.random() * size));
        firstplayerID = player.getID();
        return new PlayerDescription(player);
    }

    public int getFirstplayerID() {
        return firstplayerID;
    }

    public boolean limitObjectsOwned(Players player) {
        return player.objectCardNumber() == 4;
    }

    public void setPosition(Players player, Position position) {
        boolean move = map.setPosition(player, position);
        if (move) {
            Integer status = 1;
            changePositionEffect = true;
            setChanged();
            notifyObservers(status);
            return;
        }
        Integer status = -1;
        changePositionEffect = false;
        setChanged();
        notifyObservers(status);
        return;
    }

    public String getAttackerName() {
        return attacker.getName();
    }

    public void removeObjectCard(Integer type, Players player) {
        player.useObjectCard(type);
    }

    public Position getAttackposition() {
        return attackposition;
    }

    public void alienAttack() {
        attacker = getPlayerById(currentplayer);
        attackposition = attacker.getPosition();
        List<Integer> playersonsector = getPlayerOnSector(attackposition);
        playersDeath = new ArrayList<PlayerDescription>();
        playerDefense = new ArrayList<PlayerDescription>();
        if (playersonsector.size() == 1) {
            setChanged();
            notifyObservers(-2);
            return; //notifica a tutti che ho attaccato in attackposition
        }
        for (int i = 0; i < playersonsector.size(); i++) {
            Players player = getPlayerById(playersonsector.get(i));
            if (player instanceof AlienPlayer) {
                if (player == attacker) {
                    continue;
                }
                player.setPlayersDeath(true);
                map.removeIDFromThisPosition(attackposition, player.getID());
                playersDeath.add(new PlayerDescription(player));
                //notifica attaccato + morto
            } else if (player == attacker) {
                continue;
            } else if (player.getDefense()) {
                player.useObjectCard(4);
                player.setDefense(false);
                playerDefense.add(new PlayerDescription(player));
                List<ObjectCards> objectowned = player.getObjectsOwned();
                for (int j = 0; j < objectowned.size(); j++) {
                    if (objectowned.get(j).getType() == 4) {
                        player.setDefense(true); //notifica attacco + difesa
                    }
                }
            } else {
                player.setPlayersDeath(true);
                attacker.setAlienFeeding(true);
                map.removeIDFromThisPosition(attackposition, player.getID());
                playersDeath.add(new PlayerDescription(player)); //notifica attacco + morto
            }
        }
        //notify different results of the attack
        if (playerDefense.isEmpty()) {
            setChanged();
            notifyObservers(2);
            return;
        } else if (!playersDeath.isEmpty()) {
            setChanged();
            notifyObservers(3);
            return;
        } else if (playersDeath.isEmpty()) {
            setChanged();
            notifyObservers(6);
            return;
        }
    }

    public boolean getChangePositionEffect() {
        return changePositionEffect;
    }

    public List<PlayerDescription> getPlayersDeath() {
        return playersDeath;
    }

    public List<PlayerDescription> getPlayerDefense() {
        return playerDefense;
    }

    /**
     * Method that notify the effect of drawn sector card
     *
     * @param tipocarta
     */
    public void sectorEffect(int tipocarta) {
        switch (tipocarta) {
            case 0:
                break;
            case 1:
                setChanged();
                notifyObservers(4);
                break;
            case 2:
                setChanged();
                notifyObservers(5);
            default:
                return;
        }
    }

    public void escapeHatchEffect(EscapeHatchCards carta) {
        if (carta.isRed()) {
            getPlayerById(currentplayer).getPosition().setEscapeHatchBroken(true);
            setChanged();
            notifyObservers(carta);
        } else {
            setChanged();
            notifyObservers(carta);
        }
    }

    public void playerWin() {
        getPlayerById(currentplayer).setPlayersWin(true);
    }

    public PlayerDescription getNextPlayer() {
        if (getPlayersNumber() == currentplayer) {
            setCurrentPlayer(1);
        } else {
            setCurrentPlayer(currentplayer + 1);
        }
        return new PlayerDescription(getPlayerById(currentplayer));
    }

    /**
     * Return the number of current turn
     *
     * @return
     */
    public static int getTurnnumber() {
        return turnnumber;
    }

    /**
     * Increment the current turn index
     */
    public static void incrementTurnnumber() {
        StateGame.turnnumber++;
    }

    public int getHumanNumber() {
        int number = 0;
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i) instanceof HumanPlayer) {
                if (!players.get(i).getPlayersDeath() && !players.get(i).getPlayersWin()) {
                    number++;
                }
            }
        }
        return number;
    }

    public int getHumanWinner() {
        int number = 0;
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i) instanceof HumanPlayer) {
                if (players.get(i).getPlayersWin()) {
                    number++;
                }
            }
        }
        return number;
    }

    public int getHumanPlayer() {
        int number = 0;
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i) instanceof HumanPlayer) {
                number++;
            }
        }
        return number;
    }
}