package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * A dialog for choose the card to discard
 */

public class DiscardObject extends JDialog {
    private static final int ATTACK = 0;
    private static final int TELEPORT = 1;
    private static final int SEDATIVE = 2;
    private static final int SPOTLIGHT = 3;
    private static final int DEFENSE = 4;
    private static final int ADRENALINE = 5;
    private JPanel contentPane;
    private JButton attaccoButton;
    private JButton luciButton;
    private JButton sedativoButton;
    private JButton teletrasportoButton;
    private JButton adrenalinaButton;
    private JButton difesaButton;
    private MouseListener attack;
    private MouseListener light;
    private MouseListener sedative;
    private MouseListener teleport;
    private MouseListener adrenaline;
    private MouseListener defense;

    public DiscardObject(final Controller controller) {
        setContentPane(contentPane);
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);
        setSize(350, 350);

        attaccoButton.setEnabled(false);
        luciButton.setEnabled(false);
        sedativoButton.setEnabled(false);
        teletrasportoButton.setEnabled(false);
        adrenalinaButton.setEnabled(false);
        difesaButton.setEnabled(false);

        defense = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.discardObjectCard(DEFENSE);
                dispose();
            }
        };

        attack = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.discardObjectCard(ATTACK);
                dispose();
            }
        };

        light = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.discardObjectCard(SPOTLIGHT);
                dispose();
            }
        };

        sedative = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.discardObjectCard(SEDATIVE);
                dispose();
            }
        };

        teleport = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.discardObjectCard(TELEPORT);
                dispose();
            }
        };

        adrenaline = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.discardObjectCard(ADRENALINE);
                dispose();
            }
        };
    }

    public void setAttaccoButton(boolean active){
        attaccoButton.setEnabled(active);
    }

    public void setAttaccoListener(boolean active){
        if (active) {
            attaccoButton.addMouseListener(attack);
        }else {
            attaccoButton.removeMouseListener(attack);
        }
    }

    public void setLuciButton(boolean active){
        luciButton.setEnabled(active);
    }

    public void setLuciListener(boolean active){
        if (active) {
            luciButton.addMouseListener(light);
        }else {
            luciButton.removeMouseListener(light);
        }
    }

    public void setSedativoButton(boolean active){
        sedativoButton.setEnabled(active);
    }

    public void setSedativoListener(boolean active){
        if (active) {
            sedativoButton.addMouseListener(sedative);
        }else {
            sedativoButton.removeMouseListener(sedative);
        }
    }

    public void setTeletrasportoButton(boolean active){
        teletrasportoButton.setEnabled(active);
    }

    public void setTeletrasportoListener(boolean active){
        if (active) {
            teletrasportoButton.addMouseListener(teleport);
        }else {
            teletrasportoButton.removeMouseListener(teleport);
        }
    }

    public void setAdrenalinaButton(boolean active){
        adrenalinaButton.setEnabled(active);
    }

    public void setAdrenalinaListener(boolean active){
        if (active) {
            adrenalinaButton.addMouseListener(adrenaline);
        }else {
            adrenalinaButton.removeMouseListener(adrenaline);
        }
    }

    public void setDifesaButton(boolean active){
        difesaButton.setEnabled(active);
    }

    public void setDifesaListener(boolean active){
        if (active) {
            difesaButton.addMouseListener(defense);
        }else {
            difesaButton.removeMouseListener(defense);
        }
    }
}
