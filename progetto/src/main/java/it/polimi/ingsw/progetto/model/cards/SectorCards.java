package it.polimi.ingsw.progetto.model.cards;

/**
 * Class for sector cards
 *
 * @author Luigi Davide Greco, Daniele Guarascio
 */
public class SectorCards implements Cards {
    private boolean objecticon;
    private int type = -1;
    private static final int SILENCE = 0;
    private static final int TRUENOISE = 1;
    private static final int FAKENOISE = 2;


    /**
     * Constructor
     *
     * @param tipo defines the type of the card
     */
    public SectorCards(String tipo) {
        if (tipo == "silence") {
            type = SILENCE;
            setObjectIcon(false);
        }
        if (tipo == "truenoise") {
            type = TRUENOISE;
            setObjectIcon(false);
        }
        if (tipo == "fakenoise") {
            type = FAKENOISE;
            setObjectIcon(false);
        }

    }

    public int getType() {
        return this.type;
    }

    public String getTypeString() {
        switch (type) {
            case 0:
                return "Silenzio";
            case 1:
                return "Rumore nel tuo settore";
            case 2:
                return "Rumore in qualunque settore";
            default:
                return "";
        }
    }

    /**
     * @param type
     * @return a new sectorcards object
     */
    public static SectorCards getSectorCard(String type) {
        return new SectorCards(type);
    }

    /**
     * Method that sets objecticon
     *
     * @param oggetto true, than the player have to draw an object card
     */
    public void setObjectIcon(boolean oggetto) {
        this.objecticon = oggetto;
    }

    public boolean getObjectIcon() {
        return this.objecticon;
    }

}