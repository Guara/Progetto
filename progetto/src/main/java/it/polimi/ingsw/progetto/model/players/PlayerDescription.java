package it.polimi.ingsw.progetto.model.players;

import java.io.Serializable;

/**
 * Class that represents a summary about a player, without private info
 * @author Luigi Davide Greco, Daniele Guarascio
 *
 */
public class PlayerDescription implements Serializable {
   private static final long serialVersionUID = 3371987253116411758L;
   private final int id;
   private final String name;


   /**
    * Constructor
    * @param players object for player that I want the description
    */
   public PlayerDescription(Players players) {
      this.name = players.getName();
      this.id = players.getID();
   }

   public int getId() {
      return id;
   }

   public String getName() {
      return name;
   }
}
