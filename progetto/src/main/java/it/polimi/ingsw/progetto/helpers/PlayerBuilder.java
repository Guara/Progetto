package it.polimi.ingsw.progetto.helpers;

import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;
import it.polimi.ingsw.progetto.model.players.AlienPlayer;
import it.polimi.ingsw.progetto.model.players.HumanPlayer;
import it.polimi.ingsw.progetto.model.players.Players;

/**
 * Class that helps the creation of a player
 *
 * @author Luigi Davide Greco, Daniele Guarascio
 */
public class PlayerBuilder {
    private String name;
    private PlayerTypeCards card;

    /**
     * Constructor useful for test
     */
    public PlayerBuilder() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCard(PlayerTypeCards card) {
        this.card = card;
    }

    public Players build() {
        if (card.isAlien()) {
            return new AlienPlayer(name);
        } else {
            return new HumanPlayer(name);
        }
    }
}
