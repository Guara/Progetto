package it.polimi.ingsw.progetto.view;

import it.polimi.ingsw.progetto.controller.Controller;
import it.polimi.ingsw.progetto.model.StateGame;
import it.polimi.ingsw.progetto.model.cards.PlayerTypeCards;
import it.polimi.ingsw.progetto.helpers.PlayerBuilder;

import javax.swing.*;
import java.awt.event.*;

/**
 * A dialog for draw the Type card
 */
public class DrawTypeCard extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;

    public DrawTypeCard(final Controller controller, final StateGame model) {
        setContentPane(contentPane);
        setModal(true);
        setLocationByPlatform(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PlayerTypeCards typeCard = model.drawTypeCard(model.getTypeDeck());
                PlayerBuilder builder = View.getPlayerBuilder();
                builder.setCard(typeCard);
                model.setPlayer(builder.build());
                controller.onCardDrawn();
            }
        });
    }
}